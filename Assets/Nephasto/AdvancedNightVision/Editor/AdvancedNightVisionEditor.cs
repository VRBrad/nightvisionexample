﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEditor;

namespace Nephasto.AdvancedNightVisionAsset
{
  /// <summary>
  /// Advanced Night Vision Editor.
  /// </summary>
  [CustomEditor(typeof(AdvancedNightVision))]
  public sealed class AdvancedNightVisionEditor : Inspector
  {
    private bool foldoutRGBOffset = false;

    /// <summary>
    /// Inspector GUI.
    /// </summary>
    protected override void InspectorGUI()
    {
      BeginVertical();
      {
        Separator();

        AdvancedNightVision baseTarget = (AdvancedNightVision)target;

        BeginVertical();
        {
          /////////////////////////////////////////////////
          // General.
          /////////////////////////////////////////////////
          ToggleProperty("On / Off", "On");
          SliderProperty("SwitchOnTime");
          SliderProperty("SwitchOffTime");

          Separator();
          
          /////////////////////////////////////////////////
          // Quality.
          /////////////////////////////////////////////////
          if (Foldout("Quality") == true)
          {
            IndentLevel++;

            EnumPopupProperty("Passes", "ShaderPass");
            EnumPopupProperty("Resolution", "RenderResolution");

            if (baseTarget.ShaderPass == ShaderPasses.MultiPass)
            {
              SliderProperty("Blur", "BlurPasses");
              SliderProperty("Glow");
            }

            IndentLevel--;
          }

          Separator();

          /////////////////////////////////////////////////
          // Light management.
          /////////////////////////////////////////////////
          if (ToogleFoldoutProperty("Light Management", "AutomaticLightManagement") == true)
          {
            IndentLevel++;

            EnableGUI = GetProperty<bool>("AutomaticLightManagement");

            SliderProperty("Range", "LightRange");
            SliderProperty("Angle", "LightAngle");
            SliderProperty("Intensity", "LightIntensity");
            EnumPopupProperty("Shadow type", "LightShadow");
            ListAttribute("Ignore lights", "ignoreLights");
            
            EnableGUI = true;

            IndentLevel--;
          }

          Separator();

          /////////////////////////////////////////////////
          // Color.
          /////////////////////////////////////////////////
          if (Foldout("Color") == true)
          {
            IndentLevel++;
            
            EnumPopupProperty("Mode", "ColorControl");

            EnumPopupProperty("Color gradient", "ColorGradient");

            ToggleProperty("Invert gradient", "InvertGradient");

            if (baseTarget.ColorGradient == ColorGradients.Custom)
            {
              IndentLevel++;

              baseTarget.Tint0 = EditorGUILayout.ColorField(baseTarget.Tint0);
              baseTarget.Tint1 = EditorGUILayout.ColorField(baseTarget.Tint1);
              baseTarget.Tint2 = EditorGUILayout.ColorField(baseTarget.Tint2);
              baseTarget.Tint3 = EditorGUILayout.ColorField(baseTarget.Tint3);
              baseTarget.Tint4 = EditorGUILayout.ColorField(baseTarget.Tint4);

              IndentLevel--;
            }

            SliderProperty("Luminance", "LuminanceAmount");
            SliderProperty("Exposure");

            if (baseTarget.ColorControl == ColorControls.Advanced)
            {
              SliderProperty("Brightness");
              SliderProperty("Contrast");
              SliderProperty("Saturation");

              foldoutRGBOffset = EditorGUILayout.Foldout(foldoutRGBOffset, "RGB offset");
              if (foldoutRGBOffset == true)
              {
                IndentLevel++;

                Vector3 rgbOffset = baseTarget.RGBOffset;
                rgbOffset.x = Slider("Red", "Red offset [-100.0 - 100.0]", rgbOffset.x, -100.0f, 100.0f, 10.0f);
                rgbOffset.y = Slider("Green", "Green offset [-100.0 - 100.0]", rgbOffset.y, -100.0f, 100.0f, 1.0f);
                rgbOffset.z = Slider("Blue", "Blue offset [-100.0 - 100.0]", rgbOffset.z, -100.0f, 100.0f, -10.0f);

                baseTarget.RGBOffset = rgbOffset;

                IndentLevel--;
              }
            }

            IndentLevel--;
          }

          Separator();

          /////////////////////////////////////////////////
          // Edge.
          /////////////////////////////////////////////////
          if (ToogleFoldoutProperty("Edge detection", "Edge") == true)
          {
            IndentLevel++;

            EnableGUI = GetProperty<bool>("Edge");

            ColorProperty("Tint", "EdgeTint");
            SliderProperty("Strength", "EdgeStrength");
            
            EnableGUI = true;

            IndentLevel--;
          }

          Separator();
          
          /////////////////////////////////////////////////
          // Glitches.
          /////////////////////////////////////////////////
          if (Foldout("Glitches") == true)
          {
            IndentLevel++;
            
            EnumPopupProperty("VignetteType");
            
            if (baseTarget.VignetteType != VignetteTypes.None)
            {
              IndentLevel++;

              SliderProperty("Scale", "VignetteScale");
              SliderProperty("Softness", "VignetteSoftness");
              
              IndentLevel--;
            }

            if (baseTarget.ShaderPass == ShaderPasses.MultiPass)
            {
              EnumPopupProperty("Distortion", "ChromaticAberrationType");

              switch (baseTarget.ChromaticAberrationType)
              {
                case ChromaticAberrationTypes.Advanced:
                  IndentLevel++;

                  SliderProperty("Passes", "DistortionsPasses");
                  SliderProperty("Aberration", "ChromaticAberration");
                  SliderProperty("Barrel", "BarrelDistortion");
                
                  IndentLevel--;
                  break;
                case ChromaticAberrationTypes.Simple:
                  IndentLevel++;
                
                  SliderProperty("Aberration", "ChromaticAberration");
                
                  IndentLevel--;
                  break;
              }
            }

            if (ToggleProperty("Analog TV", "AnalogTV") == true)
            {
              IndentLevel++;

              SliderProperty("Noise");
              SliderProperty("Scanline");

              IndentLevel--;
            }

            if (ToggleProperty("Digital TV", "DigitalTV") == true)
            {
              IndentLevel++;

              LabelWidth = 135.0f;

              EditorGUILayout.LabelField("Noise");
              {
                IndentLevel++;
                
                SliderProperty("Threshold", "DigitalTVNoiseThreshold");
                SliderProperty("Max offset", "DigitalTVNoiseMaxOffset");

                IndentLevel--;
              }

              LabelWidth = 100.0f;

              IndentLevel--;
            }

            IndentLevel--;
          }

          Separator();

          /////////////////////////////////////////////////
          // UI.
          /////////////////////////////////////////////////
          if (ToogleFoldoutProperty("UI", "UI") == true)
          {
            IndentLevel++;

            Vector2Property("Center", "UICenter");

            Separator();

            Label("Cross");

            IndentLevel++;

            SliderProperty("Width", "UICrossWidth");
            ColorProperty("Color", "UICrossColor");

            IndentLevel--;

            Separator();

            Label("Grid");

            IndentLevel++;

            SliderProperty("Size", "UIGridSize");
            ColorProperty("Color", "UIGridColor");

            IndentLevel--;

            Separator();

            Label("Circles");

            IndentLevel++;

            SliderProperty("Radius 0", "UICircle0Radius");
            SliderProperty("Radius 1", "UICircle1Radius");
            SliderProperty("Radius 2", "UICircle2Radius");
            ColorProperty("Color", "UICircleColor");

            IndentLevel--;

            IndentLevel--;
          }
        }
        EndVertical();

        /////////////////////////////////////////////////
        // Misc.
        /////////////////////////////////////////////////

        Separator();

        BeginHorizontal();
        {
          if (Button("[doc]", "Online documentation", GUI.skin.label) == true)
            Application.OpenURL(AdvancedNightVision.Documentation);

          FlexibleSpace();

          if (Button("Reset") == true)
            baseTarget.ResetDefaultValues();
        }
        EndHorizontal();

        Separator();
      }
      EndVertical();
    }

    private void OnSceneGUI()
    {
      AdvancedNightVision baseTarget = (AdvancedNightVision)target;
      if (baseTarget.AutomaticLightManagement == true)
      {
        Handles.color = UnityEngine.Color.green;

        Vector3 direction = Quaternion.AngleAxis(-baseTarget.LightAngle * 0.5f, UnityEngine.Vector3.up) * baseTarget.transform.forward;
        
        Handles.DrawWireArc(baseTarget.transform.position, baseTarget.transform.up, direction, baseTarget.LightAngle, baseTarget.LightRange);
        
        baseTarget.LightRange = Handles.ScaleValueHandle(baseTarget.LightRange, baseTarget.transform.position + baseTarget.transform.forward * baseTarget.LightRange, baseTarget.transform.rotation, 4.0f, Handles.ConeHandleCap, 1);
        
        baseTarget.LightAngle = Mathf.Clamp(Handles.ScaleValueHandle(baseTarget.LightAngle, baseTarget.transform.position + baseTarget.transform.right * (Mathf.Deg2Rad * baseTarget.LightAngle * 0.8f), Quaternion.identity, 1.0f, Handles.DotHandleCap, 1), 1.0f, 179.0f);          
      }
    }
  }
}
