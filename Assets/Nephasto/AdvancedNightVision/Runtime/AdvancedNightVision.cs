﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Nephasto.AdvancedNightVisionAsset
{
  /// <summary>
  /// Advanced Night Vision.
  /// </summary>
  [RequireComponent(typeof(Camera))]
  [AddComponentMenu("Image Effects/Nephasto/Advanced Night Vision")]
  [HelpURL(Documentation)]
  [ExecuteAlways]
  public sealed class AdvancedNightVision : MonoBehaviour
  {
    /// <summary>
    /// Night Vision on / off.
    /// </summary>
    [ToggleProperty(false, "Night Vision on/off.")]
    public bool On
    {
      get { return nightVisionEnable; }
      set
      {
        if (this.enabled == true && nightVisionEnable != value)
        {
          nightVisionEnable = value;

          LightManagement();

          OnSwitch?.Invoke(nightVisionEnable);

          if (switchCoroutine != null)
            this.StopCoroutine(switchCoroutine);

          this.StartCoroutine(switchCoroutine = SwitchCoroutine());
        }
      }
    }
    
    /// <summary>
    /// Time it takes to light up. [0 - 5].
    /// </summary>
    [FloatProperty(0.0f, 5.0f, 1.0f, "Time it takes to light up.")]
    public float SwitchOnTime { get { return switchOnTime; } set { switchOnTime = Mathf.Max(0.0f, value); } }

    /// <summary>
    /// Time it takes to shut down. [0 - 5].
    /// </summary>
    [FloatProperty(0.0f, 5.0f, 0.5f, "Time it takes to shut down.")]
    public float SwitchOffTime { get { return switchOffTime; } set { switchOffTime = Mathf.Max(0.0f, value); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Quality settings.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// The number of passes used by the shader.
    /// Less passes is faster, but the quality is worse.
    /// </summary>
    [EnumProperty(1, "The number of passes used by the shader. Less passes is faster, but the quality is worse.")]
    public ShaderPasses ShaderPass
    {
      get { return shaderPasses; }
      set
      {
        if (shaderPasses != value)
        {
          shaderPasses = value;

          LoadShader();
        }
      }
    }

    /// <summary>
    /// The resolution of RenderTexture.
    /// Lower resolutions are faster, but the quality is worse.
    /// </summary>
    [EnumProperty(0, "The resolution of RenderTexture. Lower resolutions are faster, but the quality is worse.")]
    public RenderTextureResolutions RenderResolution { get { return renderTextureResolution; } set { renderTextureResolution = value; } }

    /// <summary>
    /// Blur passes [0 - 10].
    /// Less passes is faster, but the quality is worse.
    /// </summary>
    [FloatProperty(0.0f, 10.0f, 2.0f, "Blur passes. Less passes is faster, but the quality is worse.")]
    public float BlurPasses { get { return blurPasses; } set { blurPasses = (int)Mathf.Clamp(value, 0.0f, 10.0f); } }

    /// <summary>
    /// Glow, less is faster [0 - 25].
    /// </summary>
    [FloatProperty(0.0f, 25.0f, 4.0f, "Glow, less is faster")]
    public float Glow { get { return glow; } set { glow = (int)Mathf.Clamp(value, 0.0f, 25.0f); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Light management.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Enable/disable automatic light management.
    /// </summary>
    [ToggleProperty(true, "Enable/disable automatic light management.")]
    public bool AutomaticLightManagement
    {
      get { return automaticLightManagement; }
      set
      {
        if (automaticLightManagement != value)
        {
          automaticLightManagement = value;

          LightManagement();
        }
      }
    }

    /// <summary>
    /// Light range [0, 1000].
    /// </summary>
    [FloatProperty(0.0f, 1000.0f, 20.0f, "Light range.")]
    public float LightRange
    {
      get { return lightRange; }
      set
      {
        lightRange = Mathf.Max(0.0f, value);
        
        if (nightVisionLight != null)
          nightVisionLight.range = lightRange;
      }
    }

    /// <summary>
    /// Light angle [1, 179].
    /// </summary>
    [FloatProperty(1.0f, 179.0f, 110.0f, "Light angle.")]
    public float LightAngle
    {
      get { return lightAngle; }
      set
      {
        lightAngle = Mathf.Clamp(value, 1.0f, 179.0f);

        if (nightVisionLight != null)
          nightVisionLight.spotAngle = Mathf.Lerp(0.0f, lightAngle, strength);
      }
    }

    /// <summary>
    /// Light intensity [0, 100].
    /// </summary>
    [FloatProperty(0.0f, 100.0f, 1.0f, "Light intensity.")]
    public float LightIntensity
    {
      get { return lightIntensity; }
      set
      {
        lightIntensity = Mathf.Max(0.0f, value);

        if (nightVisionLight != null)
          nightVisionLight.intensity = Mathf.Lerp(0.0f, lightIntensity, strength);
      }
    }

    /// <summary>
    /// Light shadows type.
    /// </summary>
    [EnumProperty(0, "Light shadows type.")]
    public LightShadows LightShadow
    {
      get { return lightShadow; }
      set
      {
        lightShadow = value;

        if (nightVisionLight != null)
          nightVisionLight.shadows = lightShadow;
      }
    }
    
    /// <summary>
    /// Lights to ignore when switch on Night Vision.
    /// </summary>
    public List<Light> IgnoreLights { get { return ignoreLights; } set { ignoreLights = value; } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Color settings.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Color controls modes.
    /// </summary>
    [EnumProperty(1, "Color controls modes.")]
    public ColorControls ColorControl { get { return colorControlType; } set { colorControlType = value; } }

    /// <summary>
    /// Color gradient.
    /// </summary>
    [EnumProperty(0, "Color gradients.")]
    public ColorGradients ColorGradient
    {
      get { return colorGradient; }
      set
      {
        colorGradient = value;
        
        if (value != ColorGradients.Custom)
        {
          Color[] gradient = ColorGradientBank.GetColor(value);
          tint0 = gradient[0];
          tint1 = gradient[1];
          tint2 = gradient[2];
          tint3 = gradient[3];
          tint4 = gradient[4];
        }
      }
    }

    /// <summary>
    /// Gradient colors. Use only when ColorGradient == ColorGradients.Custom.
    /// </summary>
    [ColorProperty("#42FF30", "Tint color.")] public Color Tint0 { get { return tint0; } set { tint0 = value; } }
    [ColorProperty("#42FF30", "Tint color.")] public Color Tint1 { get { return tint1; } set { tint1 = value; } }
    [ColorProperty("#42FF30", "Tint color.")] public Color Tint2 { get { return tint2; } set { tint2 = value; } }
    [ColorProperty("#42FF30", "Tint color.")] public Color Tint3 { get { return tint3; } set { tint3 = value; } }
    [ColorProperty("#42FF30", "Tint color.")] public Color Tint4 { get { return tint4; } set { tint4 = value; } }

    /// <summary>
    /// Inverts the gradient color order.
    /// </summary>
    [ToggleProperty(false, "Inverts the gradient color order.")]
    public bool InvertGradient { get { return invertGradient; } set { invertGradient = value; } }

    /// <summary>
    /// Tint color amount [0.0 - 1.0].
    /// </summary>
    [FloatProperty(0.0f, 1.0f, 1.0f, "Tint color amount")]
    public float LuminanceAmount { get { return luminanceAmount; } set { luminanceAmount = Mathf.Clamp(value, 0.0f, 1.0f); } }

    /// <summary>
    /// Exposure [0.0 - 10.0].
    /// </summary>
    [FloatProperty(0.0f, 10.0f, 1.0f, "Exposure")]
    public float Exposure { get { return exposure; } set { exposure = Mathf.Clamp(value, 0.0f, 10.0f); } }

    /// <summary>
    /// Brightness [-1.0 - 1.0].
    /// </summary>
    [FloatProperty(-1.0f, 1.0f, 0.0f, "Brightness")]
    public float Brightness { get { return brightness; } set { brightness = Mathf.Clamp(value, -1.0f, 1.0f); } }

    /// <summary>
    /// Contrast [-1.0 - 1.0].
    /// </summary>
    [FloatProperty(-1.0f, 1.0f, 0.0f, "Contrast")]
    public float Contrast { get { return contrast; } set { contrast = Mathf.Clamp(value, -1.0f, 1.0f); } }

    /// <summary>
    /// Saturation [0.0 - 2.0].
    /// </summary>
    [FloatProperty(0.0f, 2.0f, 0.5f, "Saturation")]
    public float Saturation { get { return saturation; } set { saturation = Mathf.Clamp(value, 0.0f, 2.0f); } }

    /// <summary>
    /// The RGB offset to define the black and white look.
    /// </summary>
    public Vector3 RGBOffset { get { return rgbOffset; } set { rgbOffset = new Vector3(Mathf.Clamp(value.x, -100.0f, 100.0f), Mathf.Clamp(value.y, -100.0f, 100.0f), Mathf.Clamp(value.z, -100.0f, 100.0f)); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Edge detection.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Enable/disable edge detection.
    /// </summary>
    [ToggleProperty(false, "Enable/disable edge detection.")]
    public bool Edge { get { return edge; } set { edge = value; } }
    
    /// <summary>
    /// Edge tint.
    /// </summary>
    [ColorProperty("#00FFFF", "Edge tint color.")]
    public Color EdgeTint { get { return edgeTint; } set { edgeTint = value; } }
    
    /// <summary>
    /// Edge detection strength [0.0 - 5.0].
    /// </summary>
    [FloatProperty(0.0f, 5.0f, 1.0f, "Edge detection strength.")]
    public float EdgeStrength { get { return edgeStrength; } set { edgeStrength = Mathf.Max(value, 0.0f); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Vignette.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Vignette types.
    /// </summary>
    [EnumProperty(1, "Vignette types.")]
    public VignetteTypes VignetteType { get { return vignetteType; } set { vignetteType = value; } }

    /// <summary>
    /// Vignette scale [0.0 - 100.0].
    /// </summary>
    [FloatProperty(0.0f, 100.0f, 10.0f, "Vignette scale")]
    public float VignetteScale { get { return vignetteScale; } set { vignetteScale = Mathf.Clamp(value, 0.0f, 100.0f); } }

    /// <summary>
    /// Vignette softness [0.0 - 10.0].
    /// </summary>
    [FloatProperty(0.0f, 10.0f, 0.1f, "Vignette softness")]
    public float VignetteSoftness { get { return vignetteSoftness; } set { vignetteSoftness = Mathf.Clamp(value, 0.0f, 10.0f); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Distortion.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Chromatic aberration types.
    /// </summary>
    [EnumProperty(2, "Chromatic aberration types.")]
    public ChromaticAberrationTypes ChromaticAberrationType { get { return chromaticAberrationType; } set { chromaticAberrationType = value; } }

    /// <summary>
    /// Distortions passes [3 - 24].
    /// Less passes is faster, but the quality is worse.
    /// </summary>
    [IntProperty(3, 24, 6, "Distortions passes. Less passes is faster, but the quality is worse")]
    public int DistortionsPasses { get { return distortionsPasses; } set { distortionsPasses = Clamp(value, 3, 24); } }

    /// <summary>
    /// Chromatic aberration [-100.0 - 100.0].
    /// </summary>
    [FloatProperty(-100.0f, 100.0f, 2.0f, "Chromatic aberration")]
    public float ChromaticAberration { get { return chromaticAberration; } set { chromaticAberration = Mathf.Clamp(value, -100.0f, 100.0f); } }

    /// <summary>
    /// Barrel distortion [-100.0 - 100.0].
    /// </summary>
    [FloatProperty(-100.0f, 100.0f, 10.0f, "Barrel distortion")]
    public float BarrelDistortion { get { return barrelDistortion; } set { barrelDistortion = Mathf.Clamp(value, -100.0f, 100.0f); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Analog TV.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Enable/disable analog TV effects.
    /// </summary>
    [ToggleProperty(true, "Enable/disable analog TV effects.")]
    public bool AnalogTV { get { return analogTV; } set { analogTV = value; } }

    /// <summary>
    /// Noise grain [0.0 - 10.0].
    /// </summary>
    [FloatProperty(0.0f, 10.0f, 3.0f, "Noise grain")]
    public float Noise { get { return noise; } set { noise = Mathf.Clamp(value, 0.0f, 10.0f); } }

    /// <summary>
    /// Scanline [0.0 - 2.0].
    /// </summary>
    [FloatProperty(0.0f, 2.0f, 1.0f, "Scanlines")] 
    public float Scanline { get { return scanline; } set { scanline = Mathf.Clamp(value, 0.0f, 2.0f); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Digital TV.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Enable/disable digital TV effects.
    /// </summary>
    [ToggleProperty(true, "Enable/disable digital TV effects.")]
    public bool DigitalTV { get { return digitalTV; } set { digitalTV = value; } }

    /// <summary>
    /// Noise threshold.
    /// </summary>
    [FloatProperty(0.0f, 1.0f, 0.1f, "Noise threshold.")]
    public float DigitalTVNoiseThreshold { get { return digitalTVNoiseThreshold; } set { digitalTVNoiseThreshold = Mathf.Clamp01(value); } }

    /// <summary>
    /// Noise max offset.
    /// </summary>
    [FloatProperty(0.0f, 1.0f, 0.1f, "Noise max offset.")]
    public float DigitalTVNoiseMaxOffset { get { return digitalTVNoiseMaxOffset; } set { digitalTVNoiseMaxOffset = Mathf.Clamp01(value); } }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // UI.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Enable/disable UI.
    /// </summary>
    [ToggleProperty(true, "Enable/disable UI.")]
    public bool UI { get { return enableUI; } set { enableUI = value; } }

    /// <summary>
    /// UI position.
    /// </summary>
    [Vector2Property("UI position.")]
    public Vector2 UICenter { get { return uiCenter; } set { uiCenter = value; } }

    /// <summary>
    /// UI grid size.
    /// </summary>
    [FloatProperty(0.0f, 1.0f, 0.1f, "Grid size.")]
    public float UIGridSize { get { return uiGridSize; } set { uiGridSize = Mathf.Clamp01(value); } }

    /// <summary>
    /// UI grid color.
    /// </summary>
    [ColorProperty("#33E6B3", "UI grid color.")]
    public Color UIGridColor { get { return uiGridTint; } set { uiGridTint = value; } }

    /// <summary>
    /// UI circle 0 radius.
    /// </summary>
    [FloatProperty(0.0f, 1.0f, 0.2f, "UI circle 0 radius.")]
    public float UICircle0Radius { get { return uiCircle0Radius; } set { uiCircle0Radius = Mathf.Clamp01(value); } }

    /// <summary>
    /// UI circle 1 radius.
    /// </summary>
    [FloatProperty(0.0f, 1.0f, 0.5f, "UI circle 1 radius.")]
    public float UICircle1Radius { get { return uiCircle1Radius; } set { uiCircle1Radius = Mathf.Clamp01(value); } }

    /// <summary>
    /// UI circle 2 radius.
    /// </summary>
    [FloatProperty(0.0f, 1.0f, 1.0f, "UI circle 2 radius.")]
    public float UICircle2Radius { get { return uiCircle2Radius; } set { uiCircle2Radius = Mathf.Clamp01(value); } }

    /// <summary>
    /// UI circle color.
    /// </summary>
    [ColorProperty("#33E6B3", "UI circle color.")]
    public Color UICircleColor { get { return uiCircleTint; } set { uiCircleTint = value; } }

    /// <summary>
    /// UI cross width.
    /// </summary>
    [FloatProperty(0.0f, 10.0f, 1.0f, "UI cross width.")]
    public float UICrossWidth { get { return uiCrosswidth; } set { uiCrosswidth = (int)Mathf.Max(0.0f, value); } }

    /// <summary>
    /// UI cross color.
    /// </summary>
    [ColorProperty("#003300", "UI cross color.")]
    public Color UICrossColor { get { return uiCrossTint; } set { uiCrossTint = value; } }

    /// <summary>
    /// Event called when activating or deactivating the effect.
    /// </summary>
    public event SwitchAction OnSwitch;

    public delegate void SwitchAction(bool on);

    [SerializeField]
    private bool nightVisionEnable = false;

    [SerializeField]
    private float switchOnTime = 1.0f;

    [SerializeField]
    private float switchOffTime = 0.5f;
    
    [SerializeField]
    private ShaderPasses shaderPasses = ShaderPasses.MultiPass;

    [SerializeField]
    private float blurPasses = 2.0f;

    [SerializeField]
    private RenderTextureResolutions renderTextureResolution = RenderTextureResolutions.Normal;

    [SerializeField]
    private float glow = 4.0f;
    
    [SerializeField]
    private Vector3 rgbOffset = DefaultRGBOffset;

    [SerializeField]
    private float contrast = 0.0f;

    [SerializeField]
    private float saturation = 0.5f;

    [SerializeField]
    private int distortionsPasses = 6;

    [SerializeField]
    private float chromaticAberration = 2.0f;

    [SerializeField]
    private float barrelDistortion = 10.0f;

    [SerializeField]
    private bool analogTV = true;

    [SerializeField]
    private float noise = 3.0f;

    [SerializeField]
    private float scanline = 1.0f;

    [SerializeField]
    private bool digitalTV = true;

    [SerializeField]
    private ColorControls colorControlType = ColorControls.Advanced;

    [SerializeField]
    private ColorGradients colorGradient = ColorGradients.Green;

    [SerializeField]
    private bool invertGradient = false;

    [SerializeField]
    private Color tint0 = DefaultTint;
    
    [SerializeField]
    private Color tint1 = DefaultTint;
    
    [SerializeField]
    private Color tint2 = DefaultTint;
    
    [SerializeField]
    private Color tint3 = DefaultTint;
    
    [SerializeField]
    private Color tint4 = DefaultTint;

    [SerializeField]
    private float luminanceAmount = 1.0f;
    
    [SerializeField]
    private float exposure = 1.0f;
    
    [SerializeField]
    private float brightness = 0.0f;

    [SerializeField]
    private ChromaticAberrationTypes chromaticAberrationType = ChromaticAberrationTypes.Advanced;

    [SerializeField]
    private VignetteTypes vignetteType = VignetteTypes.Screen;

    [SerializeField]
    private float vignetteScale = 10.0f;

    [SerializeField]
    private float vignetteSoftness = 0.1f;

    [SerializeField]
    private bool automaticLightManagement = true;

    [SerializeField]
    private float lightRange = 20.0f;

    [SerializeField]
    private float lightAngle = 110.0f;
    
    [SerializeField]
    private float lightIntensity = 1.0f;

    [SerializeField]
    private LightShadows lightShadow = LightShadows.None;

    [SerializeField, List("Lights to ignore when switch on Night Vision.")]
    private List<Light> ignoreLights = new List<Light>();

    [SerializeField]
    private float digitalTVNoiseThreshold = 0.1f;

    [SerializeField]
    private float digitalTVNoiseMaxOffset = 0.1f;
    
    [SerializeField]
    private bool edge = false;

    [SerializeField]
    private Color edgeTint = DefaultTint;

    [SerializeField]
    private float edgeStrength = 1.0f;

    [SerializeField]
    private bool enableUI = false;

    [SerializeField]
    private Vector2 uiCenter = Vector2.zero;

    [SerializeField]
    private float uiGridSize = 0.1f;

    [SerializeField]
    private Color uiGridTint = DefaultGridTint;

    [SerializeField]
    private float uiCircle0Radius = 0.2f;

    [SerializeField]
    private float uiCircle1Radius = 0.5f;

    [SerializeField]
    private float uiCircle2Radius = 1.0f;

    [SerializeField]
    private Color uiCircleTint = DefaultGridTint;

    [SerializeField]
    private float uiCrosswidth = 1.0f;

    [SerializeField]
    private Color uiCrossTint = DefaultCrossTint;

    private readonly List<RenderTexture> renderTextures = new List<RenderTexture>();

    private float strength = 0.0f;
    
    private Material material;
    private Shader shader;
    
    private Camera effectCamera;
    private Light nightVisionLight;
    private readonly List<Light> realtimeLights = new List<Light>();
    
    private IEnumerator switchCoroutine;  

    private static readonly Vector3 DefaultRGBOffset = new Vector3(10.0f, 1.0f, -10.0f);
    private static readonly Color DefaultTint = new Color(0.258f, 1.0f, 0.188f);
    private static readonly Color DefaultCrossTint = new Color(0.0f, 0.2f, 0.0f);
    private static readonly Color DefaultGridTint = new Color(0.2f, 0.9f, 0.7f);
    private static readonly Color DefaultEdgeTint = new Color(0.0f, 1.0f, 1.0f);

    private static readonly int StrengthVariable = Shader.PropertyToID("_Strength");
    private static readonly int BlurrVariable = Shader.PropertyToID("_Blurr");
    private static readonly int ChromaticAberrationVariable = Shader.PropertyToID("_ChromaticAberration");
    private static readonly int DistortionsPassesVariable = Shader.PropertyToID("_DistortionsPasses");
    private static readonly int BarrelDistortionVariable = Shader.PropertyToID("_BarrelDistortion");
    private static readonly int GlowVariable = Shader.PropertyToID("_Glow");
    private static readonly int Tint0Variable = Shader.PropertyToID("_Tint0");
    private static readonly int Tint1Variable = Shader.PropertyToID("_Tint1");
    private static readonly int Tint2Variable = Shader.PropertyToID("_Tint2");
    private static readonly int Tint3Variable = Shader.PropertyToID("_Tint3");
    private static readonly int Tint4Variable = Shader.PropertyToID("_Tint4");
    private static readonly int LuminanceAmountVariable = Shader.PropertyToID("_LuminanceAmount");
    private static readonly int ExposureVariable = Shader.PropertyToID("_Exposure");
    private static readonly int RGBLumVariable = Shader.PropertyToID("_RGBLum");
    private static readonly int BrightnessVariable = Shader.PropertyToID("_Brightness");
    private static readonly int ContrastVariable = Shader.PropertyToID("_Contrast");
    private static readonly int SaturationVariable = Shader.PropertyToID("_Saturation");
    private static readonly int ScanlineVariable = Shader.PropertyToID("_Scanline");
    private static readonly int NoiseVariable = Shader.PropertyToID("_Noise");
    private static readonly int DigitalTVNoiseThresholdVariable = Shader.PropertyToID("_DigitalTVNoiseThreshold");
    private static readonly int DigitalTVNoiseMaxOffsetVariable = Shader.PropertyToID("_DigitalTVNoiseMaxOffset");
    private static readonly int VignetteTypeVariable = Shader.PropertyToID("_VignetteType");
    private static readonly int VignetteScaleVariable = Shader.PropertyToID("_VignetteScale");
    private static readonly int VignetteSoftnessVariable = Shader.PropertyToID("_VignetteSoftness");
    private static readonly int EdgeTintVariable = Shader.PropertyToID("_EdgeTint");
    private static readonly int EdgeStrengthVariable = Shader.PropertyToID("_EdgeStrength");
    private static readonly int UICenterVariable = Shader.PropertyToID("_UICenter");
    private static readonly int UIGridSizeVariable = Shader.PropertyToID("_UIGridSize");
    private static readonly int UIGridColorVariable = Shader.PropertyToID("_UIGridColor");
    private static readonly int UICircle0RadiusVariable = Shader.PropertyToID("_UICircle0Radius");
    private static readonly int UICircle1RadiusVariable = Shader.PropertyToID("_UICircle1Radius");
    private static readonly int UICircle2RadiusVariable = Shader.PropertyToID("_UICircle2Radius");
    private static readonly int UICircleColorVariable = Shader.PropertyToID("_UICircleColor");
    private static readonly int UICrossWidthVariable = Shader.PropertyToID("_UICrossWidth");
    private static readonly int UICrossColorVariable = Shader.PropertyToID("_UICrossColor");

    private const string KeywordBlur = "BLUR_ENABLED";
    private const string KeywordChromaticNone = "CHROMATIC_NONE";
    private const string KeywordChromaticSimple = "CHROMATIC_SIMPLE";
    private const string KeywordChromaticAdvanced = "CHROMATIC_ADVANCED";
    private const string KeywordGlow = "GLOW_ENABLED";
    private const string KeywordColorControl = "COLORCONTROL_ADVANCED";
    private const string KeywordEdge = "EDGE_ENABLED";
    private const string KeywordAnalogTV = "ANALOGTV_ENABLED";
    private const string KeywordDigitalTV = "DIGITALTV_ENABLED";
    private const string KeywordVintageNone = "VIGNETTE_NONE";
    private const string KeywordVintageScreen = "VIGNETTE_SCREEN";
    private const string KeywordVintageMonocular = "VIGNETTE_MONOCULAR";
    private const string KeywordVintageBinocular = "VIGNETTE_BINOCULAR";
    private const string KeywordUI = "UI_ENABLED";

    public const string Documentation = "https://www.nephasto.com/store/advanced-night-vision.html";

    /// <summary>
    /// Set the default values of the shader.
    /// </summary>
    public void ResetDefaultValues()
    {
      ColorGradient = ColorGradients.Green;

      blurPasses = 2.0f;
      glow = 4.0f;

      lightRange = 20.0f;
      lightAngle = 110.0f;
      lightIntensity = 1.0f;
      lightShadow = LightShadows.None;

      luminanceAmount = 1.0f;
      exposure = 1.0f;
      rgbOffset = DefaultRGBOffset;
      brightness = 0.0f;
      contrast = 0.0f;
      saturation = 0.5f;

      invertGradient = false;

      distortionsPasses = 6;
      chromaticAberration = 1.0f;
      barrelDistortion = 10.0f;

      noise = 3.0f;
      scanline = 1.0f;

      vignetteScale = 10.0f;
      vignetteSoftness = 0.1f;

      digitalTVNoiseThreshold = 0.1f;
      digitalTVNoiseMaxOffset = 0.1f;
      
      edgeTint = DefaultEdgeTint;
      edgeStrength = 1.0f;

      uiCenter = Vector2.zero;
      uiGridSize = 0.1f;
      uiGridTint = DefaultGridTint;
      uiCircle0Radius = 0.2f;
      uiCircle1Radius = 0.5f;
      uiCircle2Radius = 1.0f;
      uiCircleTint = DefaultGridTint;
      uiCrosswidth = 1.0f;
      uiCrossTint = DefaultCrossTint;
    }

    private static int Clamp(int value, int min, int max)
    {
      return (value < min) ? min : (value > max) ? max : value;
    }

    private static bool IsSupported(Shader shader, MonoBehaviour effect)
    {
      if (shader == null || shader.isSupported == false)
      {
        Debug.LogError($"[Nephasto.AdvancedNightVision] Shader not supported. '{effect}' disabled. Please contact to 'hello@nephasto.com' and send the editor's log file.");

        effect.enabled = false;
      }

      return true;
    }

    private void Awake()
    {
      LoadShader();
    }

    /// <summary>
    /// Enable the component.
    /// </summary>
    private void OnEnable()
    {
      effectCamera = this.GetComponent<Camera>();
      
      if (IsSupported(shader, this) == true)
        material = new Material(shader);

      if (nightVisionEnable == true)
      {
        nightVisionEnable = false;

        On = true;
      }
    }

    /// <summary>
    /// Destroy the component.
    /// </summary>
    private void OnDisable()
    {
      LightManagement();

      DisableNightVisionLight();
      
      if (material != null)
        DestroyImmediate(material);

      ReleaseAllTemporaryRenderTextures();
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
      if (material != null && strength > 0.0f)
      {
        material.shaderKeywords = null;
        
        material.SetFloat(StrengthVariable, strength);
        
        if (shaderPasses == ShaderPasses.MultiPass)
        {
          // Blur.
          if (blurPasses > 0.0f)
          {
            material.EnableKeyword(KeywordBlur);
            material.SetFloat(BlurrVariable, blurPasses);
          }

          switch (chromaticAberrationType)
          {
            case ChromaticAberrationTypes.None:
              material.EnableKeyword(KeywordChromaticNone);
              break;

            case ChromaticAberrationTypes.Simple:
              material.EnableKeyword(KeywordChromaticSimple);
              material.SetFloat(ChromaticAberrationVariable, chromaticAberration);
              break;

            case ChromaticAberrationTypes.Advanced:
              material.EnableKeyword(KeywordChromaticAdvanced);
              material.SetInt(DistortionsPassesVariable, distortionsPasses);
              material.SetFloat(ChromaticAberrationVariable, chromaticAberration);
              material.SetFloat(BarrelDistortionVariable, barrelDistortion * 0.05f);
              break;
          }

          // Glow.
          if (glow > 0.0f)
          {
            material.EnableKeyword(KeywordGlow);
            material.SetFloat(GlowVariable, glow);
          }
        }

        // Color gradients.
        if (invertGradient == false)
        {
          material.SetColor(Tint0Variable, tint0);
          material.SetColor(Tint1Variable, tint1);
          material.SetColor(Tint2Variable, tint2);
          material.SetColor(Tint3Variable, tint3);
          material.SetColor(Tint4Variable, tint4);
        }
        else
        {
          material.SetColor(Tint4Variable, tint0);
          material.SetColor(Tint3Variable, tint1);
          material.SetColor(Tint2Variable, tint2);
          material.SetColor(Tint1Variable, tint3);
          material.SetColor(Tint0Variable, tint4);
        }

        // Color controls.
        material.SetFloat(LuminanceAmountVariable, luminanceAmount);
        material.SetFloat(ExposureVariable, exposure);

        if (colorControlType == ColorControls.Advanced)
        {
          material.EnableKeyword(KeywordColorControl);

          material.SetVector(RGBLumVariable, rgbOffset);
          material.SetFloat(BrightnessVariable, brightness);
          material.SetFloat(ContrastVariable, 1.0f + contrast);
          material.SetFloat(SaturationVariable, saturation);
        }

        if (edge == true)
        {
          material.EnableKeyword(KeywordEdge);

          material.SetColor(EdgeTintVariable, edgeTint);
          material.SetFloat(EdgeStrengthVariable, edgeStrength);
        }

        // Analog TV (scanlines and analog noise).
        if (analogTV == true)
        {
          material.EnableKeyword(KeywordAnalogTV);
          material.SetFloat(ScanlineVariable, scanline);
          material.SetFloat(NoiseVariable, noise * 0.1f);
        }

        // Digital TV.
        if (digitalTV == true)
        {
          material.EnableKeyword(KeywordDigitalTV);

          material.SetFloat(DigitalTVNoiseThresholdVariable, digitalTVNoiseThreshold);
          material.SetFloat(DigitalTVNoiseMaxOffsetVariable, digitalTVNoiseMaxOffset);
        }

        // Vignette.
        switch (vignetteType)
        {
          case VignetteTypes.None:      material.EnableKeyword(KeywordVintageNone); break;
          case VignetteTypes.Screen:    material.EnableKeyword(KeywordVintageScreen); break;
          case VignetteTypes.Monocular: material.EnableKeyword(KeywordVintageMonocular); break;
          case VignetteTypes.Binocular: material.EnableKeyword(KeywordVintageBinocular); break;
        }

        if (vignetteType != VignetteTypes.None)
        {
          material.SetInt(VignetteTypeVariable, (int)vignetteType);
          material.SetFloat(VignetteScaleVariable, vignetteType == VignetteTypes.Screen ? vignetteScale : vignetteScale * 0.15f);
          material.SetFloat(VignetteSoftnessVariable, vignetteSoftness * 0.075f);
        }

        // UI.
        if (enableUI == true)
        {
          material.EnableKeyword(KeywordUI);

          material.SetVector(UICenterVariable, uiCenter);
          material.SetFloat(UIGridSizeVariable, 1.0f - uiGridSize);
          material.SetColor(UIGridColorVariable, uiGridTint);
          material.SetFloat(UICircle0RadiusVariable, uiCircle0Radius);
          material.SetFloat(UICircle1RadiusVariable, uiCircle1Radius);
          material.SetFloat(UICircle2RadiusVariable, uiCircle2Radius);
          material.SetColor(UICircleColorVariable, uiCircleTint);
          material.SetFloat(UICrossWidthVariable, uiCrosswidth * 2.0f);
          material.SetColor(UICrossColorVariable, uiCrossTint);
        }

        int renderTextureWidth = source.width / (int)renderTextureResolution;
        int renderTextureHeight = source.height / (int)renderTextureResolution;

        if (shaderPasses == ShaderPasses.OnePass)
        {
          // One pass.
          if (renderTextureResolution == RenderTextureResolutions.Normal)
            Graphics.Blit(source, destination, material);
          else
          {
            RenderTexture renderTexture0 = GetTemporaryRenderTexture(renderTextureWidth, renderTextureHeight);

            Graphics.Blit(source, renderTexture0, material);

            Graphics.Blit(renderTexture0, destination, material);

            ReleaseTemporaryRenderTexture(renderTexture0);
          }
        }
        else
        {
          // Multi-pass.
          RenderTexture renderTexture0 = GetTemporaryRenderTexture(renderTextureWidth, renderTextureHeight);

          Graphics.Blit(source, renderTexture0, material, 0);

          if (Math.Abs(glow) < float.Epsilon)
            Graphics.Blit(renderTexture0, destination, material, 1);
          else
          {
            RenderTexture renderTexture1 = GetTemporaryRenderTexture(renderTextureWidth, renderTextureHeight);

            Graphics.Blit(renderTexture0, renderTexture1, material, 1);

            Graphics.Blit(renderTexture1, destination, material, 2);

            ReleaseTemporaryRenderTexture(renderTexture1);
          }

          ReleaseTemporaryRenderTexture(renderTexture0);
        }
      }
      else
        Graphics.Blit(source, destination);
    }

    private void LoadShader()
    {
      shader = Resources.Load<Shader>($"Shaders/AdvancedNightVision{shaderPasses}");

      IsSupported(shader, this);

      material = new Material(shader);

      ReleaseAllTemporaryRenderTextures();
    }

    private RenderTexture GetTemporaryRenderTexture(int width, int height)
    {
      RenderTexture newRenderTexture = RenderTexture.GetTemporary(width, height, 0);

      renderTextures.Add(newRenderTexture);

      return newRenderTexture;
    }

    private void ReleaseTemporaryRenderTexture(RenderTexture renderTexture)
    {
      renderTextures.Remove(renderTexture);

      RenderTexture.ReleaseTemporary(renderTexture);
    }

    private void LightManagement()
    {
      if (automaticLightManagement == true)
      {
        if (nightVisionLight == null)
        {
          if ((nightVisionLight = this.gameObject.GetComponent<Light>()) == null)
          {
            nightVisionLight = this.gameObject.AddComponent<Light>();
            nightVisionLight.hideFlags = HideFlags.HideInInspector;
          }

          nightVisionLight.type = LightType.Spot;
        }

        if (nightVisionEnable == true)
        {
          realtimeLights.Clear();

          Light[] allLights = FindObjectsOfType<Light>();
          for (int i = 0; i < allLights.Length; ++i)
          {
            if (allLights[i].enabled == true &&
                allLights[i].bakingOutput.isBaked == false &&
                ignoreLights.Contains(allLights[i]) == false &&
                allLights[i] != nightVisionLight)
            {
              allLights[i].enabled = false;

              realtimeLights.Add(allLights[i]);
            }
          }

          nightVisionLight.range = lightRange;
          nightVisionLight.spotAngle = lightAngle;
          nightVisionLight.intensity = lightIntensity;
          nightVisionLight.shadows = lightShadow;
          nightVisionLight.bounceIntensity = 0.0f;
          nightVisionLight.enabled = strength > 0.0f && this.enabled == true;
        }
        else
          DisableNightVisionLight();
      }
      else if (nightVisionLight != null)
      {
        nightVisionLight.enabled = false;

        for (int i = 0; i < realtimeLights.Count; ++i)
          realtimeLights[i].enabled = true;

        realtimeLights.Clear();
      }
    }

    private void ChangeStrength(float value)
    {
      strength = Mathf.Clamp01(value);

      if (nightVisionLight != null)
      {
        nightVisionLight.enabled = strength > 0.0f && this.enabled == true;
        nightVisionLight.intensity = Mathf.Lerp(0.0f, lightIntensity, strength);
        nightVisionLight.spotAngle = Mathf.Lerp(0.0f, lightAngle, strength);
      }
    }
    
    private IEnumerator SwitchCoroutine()
    {
      float time = nightVisionEnable == true ? switchOnTime : switchOffTime;
      while (time > 0.0f)
      {
        ChangeStrength(nightVisionEnable == true ? (1.0f - (time / switchOnTime)) : time / switchOffTime);
        
        time -= Time.deltaTime;
        
        yield return null;
      }

      ChangeStrength(nightVisionEnable == true ? 1.0f : 0.0f);
    }

    private void DisableNightVisionLight()
    {
      if (nightVisionLight != null)
        nightVisionLight.enabled = false;

      for (int i = 0; i < realtimeLights.Count; ++i)
        realtimeLights[i].enabled = true;

      realtimeLights.Clear();
    }

    private void ReleaseAllTemporaryRenderTextures()
    {
      while (renderTextures.Count > 0)
      {
        int idx = renderTextures.Count - 1;
        RenderTexture renderTexture = renderTextures[idx];

        renderTextures.RemoveAt(idx);
        RenderTexture.ReleaseTemporary(renderTexture);
      }
    }
  }
}
