﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Nephasto.AdvancedNightVisionAsset
{
	/// <summary>
	/// The resolution of RenderTexture.
	/// Lower resolutions are faster, but the quality is worse.
	/// </summary>
	public enum RenderTextureResolutions
	{
		/// <summary>
		/// Same as screen (default).
		/// </summary>
		Normal = 1,

		/// <summary>
		/// Half the resolution
		/// </summary>
		Half = 2,

		/// <summary>
		/// Quarter the resolution
		/// </summary>
		Quarter = 4,
	}
}
