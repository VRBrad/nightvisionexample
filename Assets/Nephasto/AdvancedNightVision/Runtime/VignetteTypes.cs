﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Nephasto.AdvancedNightVisionAsset
{
	/// <summary>
	/// Vignette type.
	/// </summary>
	public enum VignetteTypes
	{
		/// <summary>
		/// Disabled.
		/// </summary>
		None,

		/// <summary>
		/// Dark edges of the screen.
		/// </summary>
		Screen,

		/// <summary>
		/// Monocular.
		/// </summary>
		Monocular,

		/// <summary>
		/// Binocular.
		/// </summary>
		Binocular,
	}
}
