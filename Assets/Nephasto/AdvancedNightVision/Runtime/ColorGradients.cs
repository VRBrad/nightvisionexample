﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;

namespace Nephasto.AdvancedNightVisionAsset
{
  /// <summary>
  /// Color gradients.
  /// </summary>
  public enum ColorGradients
  {
    /// <summary>
    /// Classic night vision green.
    /// </summary>
    Green,

    /// <summary>
    /// More bright green.
    /// </summary>
    BrightGreen,

    /// <summary>
    /// Lime gradient.
    /// </summary>
    Lime,

    /// <summary>
    /// Emerald green.
    /// </summary>
    Emerald,

    /// <summary>
    /// Cyan gradient.
    /// </summary>
    Cyan,

    /// <summary>
    /// Purple gradient.
    /// </summary>
    Purple,

    /// <summary>
    /// Brown gradient.
    /// </summary>
    Brown,

    /// <summary>
    /// Red gradient.
    /// </summary>
    Red,

    /// <summary>
    /// Black and white gradient.
    /// </summary>
    BlackAndWhite,

    /// <summary>
    /// Fake thermal vision.
    /// </summary>
    Thermal,

    /// <summary>
    /// Custom color gradient (use Tint0, Tint1, Tint2, Tint3 and Tint4 properties).
    /// </summary>
    Custom,
  }

  /// <summary>
  /// Color gradients.
  /// </summary>
  public static class ColorGradientBank
  {
    /// <summary>
    /// Enum to color array.
    /// </summary>
    /// <param name="gradient">Color gradient.</param>
    /// <returns>Color array.</returns>
    public static Color[] GetColor(ColorGradients gradient)
    {
      switch (gradient)
      {
        case ColorGradients.Green:          return Green;
        case ColorGradients.BrightGreen:    return BrightGreen;
        case ColorGradients.Lime:           return Lime;
        case ColorGradients.Emerald:        return Emerald;
        case ColorGradients.Cyan:           return Cyan;
        case ColorGradients.Purple:         return Purple;
        case ColorGradients.Brown:          return Brown;
        case ColorGradients.Red:            return Red;
        case ColorGradients.BlackAndWhite:  return BlackAndWhite;
        case ColorGradients.Thermal:        return Thermal;
      }

      return Green;
    }
    
    private static Color[] Emerald = { new Color(13.0f / 255.0f,  13.0f / 255.0f,  13.0f / 255.0f),
                                       new Color( 2.0f / 255.0f,  89.0f / 255.0f,  64.0f / 255.0f),
                                       new Color( 3.0f / 255.0f, 140.0f / 255.0f, 101.0f / 255.0f),
                                       new Color( 4.0f / 255.0f, 217.0f / 255.0f, 157.0f / 255.0f),
                                       new Color( 4.0f / 255.0f, 217.0f / 255.0f, 178.0f / 255.0f) };

    private static Color[] BrightGreen = { new Color(  4.0f / 255.0f,  38.0f / 255.0f,  29.0f / 255.0f),
                                           new Color( 14.0f / 255.0f,  59.0f / 255.0f,  26.0f / 255.0f),
                                           new Color( 73.0f / 255.0f, 140.0f / 255.0f,  55.0f / 255.0f),
                                           new Color(159.0f / 255.0f, 242.0f / 255.0f, 121.0f / 255.0f),
                                           new Color(196.0f / 255.0f, 242.0f / 255.0f, 174.0f / 255.0f) };

    private static Color[] Green = { new Color(  2.0f / 255.0f,  38.0f / 255.0f, 1.0f / 255.0f),
                                     new Color(  3.0f / 255.0f,  64.0f / 255.0f, 1.0f / 255.0f),
                                     new Color(  6.0f / 255.0f, 115.0f / 255.0f, 2.0f / 255.0f),
                                     new Color(  9.0f / 255.0f, 166.0f / 255.0f, 3.0f / 255.0f),
                                     new Color( 10.0f / 255.0f, 191.0f / 255.0f, 4.0f / 255.0f) };

    private static Color[] Lime = { new Color( 23.0f / 255.0f,  23.0f / 255.0f, 23.0f / 255.0f),
                                    new Color( 46.0f / 255.0f,  46.0f / 255.0f, 46.0f / 255.0f),
                                    new Color( 77.0f / 255.0f, 204.0f / 255.0f,  0.0f / 255.0f),
                                    new Color(128.0f / 255.0f, 255.0f / 255.0f,  0.0f / 255.0f),
                                    new Color(204.0f / 255.0f, 255.0f / 255.0f,  0.0f / 255.0f) };

    private static Color[] BlackAndWhite = { new Color(  0.0f / 255.0f,   8.0f / 255.0f,  0.0f / 255.0f),
                                             new Color( 30.0f / 255.0f,  47.0f / 255.0f, 29.0f / 255.0f),
                                             new Color(113.0f / 255.0f, 128.0f / 255.0f,121.0f / 255.0f),
                                             new Color(134.0f / 255.0f, 148.0f / 255.0f,131.0f / 255.0f),
                                             new Color(230.0f / 255.0f, 230.0f / 255.0f,230.0f / 255.0f) };

    private static Color[] Cyan = { new Color(  5.0f / 255.0f,  32.0f / 255.0f, 38.0f / 255.0f),
                                    new Color( 14.0f / 255.0f,  89.0f / 255.0f, 89.0f / 255.0f),
                                    new Color( 31.0f / 255.0f, 140.0f / 255.0f,140.0f / 255.0f),
                                    new Color( 50.0f / 255.0f, 166.0f / 255.0f,154.0f / 255.0f),
                                    new Color(115.0f / 255.0f, 217.0f / 255.0f,207.0f / 255.0f) };

    private static Color[] Thermal = { new Color( 17.0f / 255.0f,  84.0f / 255.0f,218.0f / 255.0f),
                                       new Color( 19.0f / 255.0f, 138.0f / 255.0f,242.0f / 255.0f),
                                       new Color( 17.0f / 255.0f, 174.0f / 255.0f,191.0f / 255.0f),
                                       new Color(235.0f / 255.0f, 242.0f / 255.0f, 12.0f / 255.0f),
                                       new Color(242.0f / 255.0f,  12.0f / 255.0f, 12.0f / 255.0f) };

    private static Color[] Purple = { new Color(  2.0f / 255.0f,  14.0f / 255.0f,  3.0f / 255.0f),
                                      new Color( 10.0f / 255.0f,  49.0f / 255.0f,114.0f / 255.0f),
                                      new Color( 83.0f / 255.0f,  17.0f / 255.0f,164.0f / 255.0f),
                                      new Color(193.0f / 255.0f,  36.0f / 255.0f,177.0f / 255.0f),
                                      new Color(226.0f / 255.0f, 103.0f / 255.0f,235.0f / 255.0f) };

    private static Color[] Brown = { new Color( 64.0f / 255.0f,  19.0f / 255.0f, 15.0f / 255.0f),
                                     new Color(166.0f / 255.0f,  34.0f / 255.0f,  5.0f / 255.0f),
                                     new Color(242.0f / 255.0f,  92.0f / 255.0f,  5.0f / 255.0f),
                                     new Color(242.0f / 255.0f, 181.0f / 255.0f, 68.0f / 255.0f),
                                     new Color(242.0f / 255.0f, 238.0f / 255.0f,172.0f / 255.0f) };
    
    public static Color[] Red = { new Color( 69.0f / 255.0f,   0.0f / 255.0f,  3.0f / 255.0f),
                                  new Color( 92.0f / 255.0f,   0.0f / 255.0f,  2.0f / 255.0f),
                                  new Color(148.0f / 255.0f,   9.0f / 255.0f, 13.0f / 255.0f),
                                  new Color(205.0f / 255.0f,  13.0f / 255.0f, 35.0f / 255.0f),
                                  new Color(255.0f / 255.0f,  29.0f / 255.0f, 35.0f / 255.0f) };
  }
}
