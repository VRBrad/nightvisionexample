﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;

using UnityEngine;

namespace Nephasto.AdvancedNightVisionAsset
{
  /// <summary>
  /// Boolean attribute.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class ToggleAttribute : Attribute
  {
    public readonly bool defaultValue;

    public readonly string tooltip;

    public ToggleAttribute(bool defaultValue, string tooltip = "")
    {
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }

  /// <summary>
  /// Boolean property.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class ToggleProperty : PropertyAttribute
  {
    public readonly bool defaultValue;

    public readonly string tooltip;

    public ToggleProperty(bool defaultValue, string tooltip = "")
    {
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }
  
  /// <summary>
  /// Int attribute.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class IntAttribute : Attribute
  {
    public readonly int min;
    public readonly int max;
    public readonly int defaultValue;

    public readonly string tooltip;

    public IntAttribute(int min, int max, int defaultValue, string tooltip = "")
    {
      this.min = min;
      this.max = max;
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }

  /// <summary>
  /// Int property.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class IntProperty : PropertyAttribute
  {
    public readonly int min;
    public readonly int max;
    public readonly int defaultValue;

    public readonly string tooltip;

    public IntProperty(int min, int max, int defaultValue, string tooltip = "")
    {
      this.min = min;
      this.max = max;
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }
  
  /// <summary>
  /// Float attribute.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class FloatAttribute : Attribute
  {
    public readonly float min;
    public readonly float max;
    public readonly float defaultValue;

    public readonly string tooltip;

    public FloatAttribute(string tooltip = "")
    {
      min = max = defaultValue = 0.0f;
      this.tooltip = tooltip;
    }

    public FloatAttribute(float min, float max, float defaultValue, string tooltip = "")
    {
      this.min = min;
      this.max = max;
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }

  /// <summary>
  /// Float property.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class FloatProperty : PropertyAttribute
  {
    public readonly float min;
    public readonly float max;
    public readonly float defaultValue;

    public readonly string tooltip;

    public FloatProperty(string tooltip = "")
    {
      min = max = defaultValue = 0.0f;
      this.tooltip = tooltip;
    }

    public FloatProperty(float min, float max, float defaultValue, string tooltip = "")
    {
      this.min = min;
      this.max = max;
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }

  /// <summary>
  /// Vector2 property.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class Vector2Property : PropertyAttribute
  {
    public readonly Vector2 defaultValue;

    public readonly string tooltip;

    public Vector2Property(string tooltip = "")
    {
      defaultValue = Vector2.zero;
      this.tooltip = tooltip;
    }

    public Vector2Property(Vector2 defaultValue, string tooltip = "")
    {
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }
  
  /// <summary>
  /// Enum attribute.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class EnumAttribute : Attribute
  {
    public readonly int defaultValue;
    
    public readonly string tooltip;

    public EnumAttribute(string tooltip = "")
    {
      this.defaultValue = 0;
      this.tooltip = tooltip;
    }

    public EnumAttribute(int defaultValue, string tooltip = "")
    {
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }

  /// <summary>
  /// Enum property.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class EnumProperty : PropertyAttribute
  {
    public readonly int defaultValue;
    
    public readonly string tooltip;

    public EnumProperty(string tooltip = "")
    {
      this.defaultValue = 0;
      this.tooltip = tooltip;
    }

    public EnumProperty(int defaultValue, string tooltip = "")
    {
      this.defaultValue = defaultValue;
      this.tooltip = tooltip;
    }
  }
  
  /// <summary>
  /// Color attribute.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class ColorAttribute : Attribute
  {
    public readonly Color defaultValue;

    public readonly string tooltip;

    public ColorAttribute(string defaultValue, string tooltip = "")
    {
      ColorUtility.TryParseHtmlString(defaultValue, out this.defaultValue);
      this.tooltip = tooltip;
    }
  }

  /// <summary>
  /// Color property.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class ColorProperty : PropertyAttribute
  {
    public readonly Color defaultValue;

    public readonly string tooltip;

    public ColorProperty(string defaultValue, string tooltip = "")
    {
      ColorUtility.TryParseHtmlString(defaultValue, out this.defaultValue);
      this.tooltip = tooltip;
    }
  }
  
  /// <summary>
  /// List attribute.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class ListAttribute : Attribute
  {
    public readonly string tooltip;

    public ListAttribute(string tooltip = "")
    {
      this.tooltip = tooltip;
    }
  }
  
  /// <summary>
  /// List property.
  /// </summary>
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
  public sealed class ListProperty : PropertyAttribute
  {
    public readonly string tooltip;

    public ListProperty(string tooltip = "")
    {
      this.tooltip = tooltip;
    }
  }
}
