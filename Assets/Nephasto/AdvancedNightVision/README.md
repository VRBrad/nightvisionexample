# Advanced Night Vision

A highly configurable, AAA quality and easy to use night vision effect.

## Getting Started

After install, read the documentation at folder Nephasto/AdvancedNightVision/Documentation or visit the [online version](https://www.nephasto.com/store/advanced-night-vision.html).

## Requisites

Unity 2019.4 or higher (URP / HDRP soon).

## Running the demo

Open the scene at folder Nephasto/AdvancedNightVision/Demo/Scenes. You can delete the folder Nephasto/AdvancedNightVision/Demo if you want.

## Bugs and/or questions

Please send us an email to hello@nephasto.com.

## Authors

* **Martin Bustos** - [@Nephasto](https://twitter.com/Nephasto)

## Thanks

* Johan from digitalsoftware.se

## License

See the [Asset Store Terms of Service and EULA](https://unity3d.com/legal/as_terms) file for details.
