﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Shader "Hidden/Advanced Night Vision - Three passes"
{
  Properties
  {
    _MainTex("Base (RGB)", 2D) = "white" {}
  }
  
  CGINCLUDE
  #include "UnityCG.cginc"
  #include "AdvancedNightVisionCG.cginc"
  
  float _Blur;
  
  float _Strength;
  float _LuminanceAmount;
  float _Exposure;
  float3 _RGBLum;
  float3 _Tint0, _Tint1, _Tint2, _Tint3, _Tint4;
  float _Brightness;
  float _Contrast;
  float _Saturation;
  
  int _DistortionsPasses;
  float _ChromaticAberration;
  float _BarrelDistortion;
  
  float _Noise;
  float _Scanline;
  
  int _VignetteType;
  float _VignetteScale;
  float _VignetteSoftness;
  
  float _Glow;
  
  float _DigitalTVNoiseThreshold;
  float _DigitalTVNoiseMaxOffset;
  
  float3 _EdgeTint;
  float _EdgeStrength;
  
  inline float Scanline(float2 uv)
  {
    return sin(_ScreenParams.y * uv.y * _Scanline);
  }
  
  inline float VignetteScreen(float2 uv)
  {
    uv = (uv - 0.5) * 0.98;
  
    return clamp(pow(cos(uv.x * 3.1415), _VignetteScale * 0.25) * pow(cos(uv.y * 3.1415), _VignetteScale * 0.25) / _VignetteSoftness, 0.0, 1.0);
  }
  
  inline float VignetteMonocular(float2 uv)
  {
    float radius = distance(float2(uv.x, (uv.y - 0.5) * (_ScreenParams.y / _ScreenParams.x) + 0.5), 0.5) * _VignetteScale;
  
    if (radius > 0.5 - _VignetteSoftness)
    {
      if (radius < 0.5)
        return pow(1.0 - ((radius - (0.5 - _VignetteSoftness)) / _VignetteSoftness), 1.5);
      else
        return 0.0;
    }
  
    return 1.0;
  }
  
  inline float VignetteBinocular(float2 uv)
  {
    float ratio = _ScreenParams.y / _ScreenParams.x;
    float radius = distance(float2(uv.x, (uv.y - 0.5) * ratio + 0.5), float2(0.25, 0.5)) * _VignetteScale;
    
    float left = 1.0;
    if (radius > 0.5 - _VignetteSoftness)
    {
      if (radius < 0.5)
        left = pow(1.0 - ((radius - (0.5 - _VignetteSoftness)) / _VignetteSoftness), 1.5);
      else
        left = 0.0;
    }
  
    radius = distance(float2(uv.x, (uv.y - 0.5) * ratio + 0.5), float2(0.75, 0.5)) * _VignetteScale;
  
    float right = 1.0;
    if (radius > 0.5 - _VignetteSoftness)
    {
      if (radius < 0.5)
        right = pow(1.0 - ((radius - (0.5 - _VignetteSoftness)) / _VignetteSoftness), 1.5);
      else
        right = 0.0;
    }

    return 1.0 - (1.0 - left) * (1.0 - right);
  }
  
  inline float2 BarrelDistortion(float2 coord, float amt)
  {
    float2 cc = coord - 0.5;
  
    float distortion = dot(cc * _BarrelDistortion * 0.3, cc);
  
    return coord + cc * distortion * -1.0 * amt;
  }
  
  float4 fragPass0(v2f_img i) : COLOR
  {
    UNITY_SETUP_INSTANCE_ID(i);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
  
    float3 pixel = SampleMainTexture(i.uv);
  
    float2 uv = i.uv;
    
    // Blur.
    float3 blur_bgy = 0.0;
#ifdef BLUR_ENABLED
    float3 accu = 0.0;
    float energy = 0.0;
    
    int intBlur = int(_Blur);
    for (int x = -intBlur; x <= intBlur; ++x)
    {
      for (int y = -intBlur; y <= intBlur; ++y)
      {
        float fx = float(x);
        float fy = float(y);
    
        energy += (1.0 - (abs(fx) / _Blur)) + (1.0 - (abs(fy) / _Blur));
        float anEnergy = (1.0 - (abs(fx) / _Blur)) + (1.0 - (abs(fy) / _Blur));
    
        accu += SampleMainTexture(float2(uv.x + fx / _ScreenParams.x, uv.y + fy / _ScreenParams.y)) * anEnergy;
      }
    }
    
    blur_bgy = energy > 0.0 ? (accu / energy) : SampleMainTexture(uv);
#else
    blur_bgy = SampleMainTexture(uv);
#endif
    
    // Luminance.
    const float3 lumcoeff = float3(0.2126, 0.7152, 0.0722);
    
    float3 final = blur_bgy * (exp2(blur_bgy) * _Exposure);
    
    // Color.
#ifdef COLORCONTROL_ADVANCED
    float4 RGB_lum = float4(lumcoeff * _RGBLum, 0.0);
    final = dot(final, RGB_lum);
    
    if (final.r < 0.25)
      final.rgb = lerp(_Tint0, _Tint1, final.r * 4.0);
    else if (final.r < 0.5)
      final.rgb = lerp(_Tint1, _Tint2, (final.r - 0.25) * 4.0);
    else if (final.r < 0.75)
      final.rgb = lerp(_Tint2, _Tint3, (final.r - 0.5) * 4.0);
    else
      final.rgb = lerp(_Tint3, _Tint4, min(final.r - 0.75, 0.25) * 4.0);

    float3 intensity = dot(final, lumcoeff);
    final = lerp(intensity, final, _Saturation);
    final = (final - 0.5f) * _Contrast + 0.5f + _Brightness;
#else
    final = dot(final, lumcoeff);

    if (final.r < 0.25)
      final.rgb = lerp(_Tint0, _Tint1, final.r * 4.0);
    else if (final.r < 0.5)
      final.rgb = lerp(_Tint1, _Tint2, (final.r - 0.25) * 4.0);
    else if (final.r < 0.75)
      final.rgb = lerp(_Tint2, _Tint3, (final.r - 0.5) * 4.0);
    else
      final.rgb = lerp(_Tint3, _Tint4, min(final.r - 0.75, 0.25) * 4.0);
#endif
    
    final = lerp(blur_bgy, final, _LuminanceAmount);
    
    // Edge.
#ifdef EDGE_ENABLED
    float gx = 0.0;
    gx += -1.0 * Lookup(uv, -1.0, -1.0, _EdgeStrength);
    gx += -2.0 * Lookup(uv, -1.0,  0.0, _EdgeStrength);
    gx += -1.0 * Lookup(uv, -1.0,  1.0, _EdgeStrength);
    gx +=  1.0 * Lookup(uv,  1.0, -1.0, _EdgeStrength);
    gx +=  2.0 * Lookup(uv,  1.0,  0.0, _EdgeStrength);
    gx +=  1.0 * Lookup(uv,  1.0,  1.0, _EdgeStrength);

    float gy = 0.0;
    gy += -1.0 * Lookup(uv, -1.0, -1.0, _EdgeStrength);
    gy += -2.0 * Lookup(uv,  0.0, -1.0, _EdgeStrength);
    gy += -1.0 * Lookup(uv,  1.0, -1.0, _EdgeStrength);
    gy +=  1.0 * Lookup(uv, -1.0,  1.0, _EdgeStrength);
    gy +=  2.0 * Lookup(uv,  0.0,  1.0, _EdgeStrength);
    gy +=  1.0 * Lookup(uv,  1.0,  1.0, _EdgeStrength);
    
    final += (gx * gx + gy * gy) * _EdgeTint;
#endif

    return float4(lerp(pixel, final, _Strength), 1.0);
  }
  
  float4 fragPass1(v2f_img i) : COLOR
  {
    UNITY_SETUP_INSTANCE_ID(i);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

    float4 pixel = float4(SampleMainTexture(i.uv), 1.0);
    
    // Digital TV.
#ifdef DIGITALTV_ENABLED
    float2 uv = DigitalTVNoise(i.uv, _DigitalTVNoiseThreshold, _DigitalTVNoiseMaxOffset);
#else
    float2 uv = i.uv;
#endif
    float4 final = (float4)0;
    
    // Chromatic aberration.
#ifdef CHROMATIC_NONE
    final = float4(SampleMainTexture(uv), 1.0);
#elif CHROMATIC_SIMPLE
    float2 coords = (uv - 0.5) * 2.0;
    float2 uvg = uv - _MainTex_TexelSize.xy * _ChromaticAberration * coords * dot(coords, coords);
    
    final = float4(SampleMainTexture(uv), 1.0);
    final.g = SampleMainTexture(uvg).g;
#elif CHROMATIC_ADVANCED
    float3 sumcol = 0.0;
    float3 sumw = 0.0;
    
    for (int i = 0; i < _DistortionsPasses; ++i)
    {
      float t = float(i) * (1.0 / float(_DistortionsPasses));
      float3 w = SpectrumOffset(t);
      sumw += w;
      sumcol += w * SampleMainTexture(BarrelDistortion(uv, _ChromaticAberration * t)).rgb;
    }
    
    final = float4(sumcol / sumw, 1.0);
#endif
    
    // Analog TV.
#ifdef ANALOGTV_ENABLED
    final.rgb *= lerp(Scanline(uv), final.rgb, 0.75);
    
    float3 grain = Noise(uv);
    
    grain = lerp(0.5, grain, _Noise);
    grain = grain.r;
    final.rgb = Overlay(grain, final);
#else
    final.rgb *= lerp(0.0, final.rgb, 0.75);
#endif
    
    // Vignette.
    float vignette = pow(4.0 * uv.y * (1.0 - uv.y), 0.15);
#ifdef VIGNETTE_SCREEN
    vignette *= VignetteScreen(uv);
#elif VIGNETTE_MONOCULAR
    vignette *= VignetteMonocular(uv);
#elif VIGNETTE_BINOCULAR
    vignette *= VignetteBinocular(uv);
#endif
    final *= vignette;

#ifdef UI_ENABLED
    final.rgb = lerp(final.rgb, DrawUI(final.rgb, uv), vignette);
#endif

    final = clamp(final, 0.0, 1.0);

    final.rgb = Screen(final, final);
    
    return lerp(pixel, final, _Strength);
  }
  
  float4 fragPass2(v2f_img i) : COLOR
  {
    UNITY_SETUP_INSTANCE_ID(i);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

    float2 uv = i.uv;
    
    float3 pixel = SampleMainTexture(uv);
    float3 final = pixel;

    // Glow.
    float3 finalGlow = 0.0;
#ifdef GLOW_ENABLED
    int intGlow = int(_Glow);
    float3 accu = 0.0;
    float energy = 0.0;
    
    for (int x = -intGlow; x <= intGlow; ++x)
    {
      for (int y = -intGlow; y <= intGlow; ++y)
      {
        float fx = float(x);
        float fy = float(y);
      
        energy += (1.0 - (abs(fx) / _Glow)) + (1.0 - (abs(fy) / _Glow));
      
        accu += SampleMainTexture(float2(uv.x + (fx / _ScreenParams.x), uv.y + (fy / _ScreenParams.y)));
      }
    }
    
    finalGlow = energy > 0.0 ? (accu / energy) : final;
#else
    finalGlow = final;
#endif

    return float4(lerp(pixel, Screen(final, finalGlow), _Strength), 1.0);
  }
  
  ENDCG
  
  SubShader
  {
    ZTest Always
    Cull Off
    ZWrite Off
    Fog { Mode off }
    
    // Pass 0: Blur and luminance.
    Pass
    {
      Name "Avanced Night Vision, pass 0"

      CGPROGRAM
      #pragma target 3.0
      #pragma exclude_renderers d3d9
      #pragma fragmentoption ARB_precision_hint_fastest
      
      #pragma multi_compile ___ BLUR_ENABLED
      #pragma multi_compile ___ COLORCONTROL_ADVANCED
      #pragma multi_compile ___ EDGE_ENABLED
     
      #pragma vertex vert_img
      #pragma fragment fragPass0
      ENDCG
    }
    
    // Pass 1: Vignette and glitches.
    Pass
    {
      Name "Avanced Night Vision, pass 1"

      CGPROGRAM
      #pragma target 3.0
      #pragma exclude_renderers d3d9
      #pragma fragmentoption ARB_precision_hint_fastest
      
      #pragma multi_compile ___ CHROMATIC_NONE CHROMATIC_SIMPLE CHROMATIC_ADVANCED
      #pragma multi_compile ___ VIGNETTE_NONE VIGNETTE_SCREEN VIGNETTE_MONOCULAR VIGNETTE_BINOCULAR
      #pragma multi_compile ___ ANALOGTV_ENABLED
      #pragma multi_compile ___ DIGITALTV_ENABLED
      #pragma multi_compile ___ UI_ENABLED
      
      #pragma vertex vert_img
      #pragma fragment fragPass1
      ENDCG
    }
      
    // Pass 2: Glow.
    Pass
    {
      Name "Avanced Night Vision, pass 2"

      CGPROGRAM
      #pragma target 3.0
      #pragma exclude_renderers d3d9
      #pragma fragmentoption ARB_precision_hint_fastest
      
      #pragma multi_compile ___ GLOW_ENABLED

      #pragma vertex vert_img
      #pragma fragment fragPass2
      ENDCG
    }
  }

  Fallback "Hidden/Advanced Night Vision - One pass"
}