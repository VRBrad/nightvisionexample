﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Shader "Hidden/Advanced Night Vision - One pass"
{
  Properties
  {
    _MainTex("Base (RGB)", 2D) = "white" {}
  }

  CGINCLUDE
  #include "UnityCG.cginc"
  #include "AdvancedNightVisionCG.cginc"

  float _Strength;
  float _LuminanceAmount;
  float _Exposure;
  float3 _RGBLum;
  float3 _Tint0, _Tint1, _Tint2, _Tint3, _Tint4;
  float _Brightness;
  float _Contrast;
  float _Saturation;

  float _Noise;
  float _Scanline;

  int _VignetteType;
  float _VignetteScale;
  float _VignetteSoftness;

  float _DigitalTVNoiseThreshold;
  float _DigitalTVNoiseMaxOffset;

  float3 _EdgeTint;
  float _EdgeStrength;

  inline float Scanline(float2 uv)
  {
    return sin(_ScreenParams.y * uv.y * _Scanline);
  }

  inline float VignetteScreen(float2 uv)
  {
    uv = (uv - 0.5) * 0.98;

    return clamp(pow(cos(uv.x * 3.1415), _VignetteScale * 0.25) * pow(cos(uv.y * 3.1415), _VignetteScale * 0.25) / _VignetteSoftness, 0.0, 1.0);
  }

  inline float VignetteMonocular(float2 uv)
  {
    float radius = distance(float2(uv.x, (uv.y - 0.5) * (_ScreenParams.y / _ScreenParams.x) + 0.5), 0.5) * _VignetteScale;

    if (radius > 0.5 - _VignetteSoftness)
    {
      if (radius < 0.5)
        return pow(1.0 - ((radius - (0.5 - _VignetteSoftness)) / _VignetteSoftness), 1.5);
      else
        return 0.0;
    }

    return 1.0;
  }

  inline float VignetteBinocular(float2 uv)
  {
    float ratio = _ScreenParams.y / _ScreenParams.x;

    float radius = distance(float2(uv.x, (uv.y - 0.5) * ratio + 0.5), float2(0.25, 0.5)) * _VignetteScale;

    float left = 1.0;
    if (radius > 0.5 - _VignetteSoftness)
    {
      if (radius < 0.5)
        left = pow(1.0 - ((radius - (0.5 - _VignetteSoftness)) / _VignetteSoftness), 1.5);
      else
        left = 0.0;
    }

    radius = distance(float2(uv.x, (uv.y - 0.5) * ratio + 0.5), float2(0.75, 0.5)) * _VignetteScale;

    float right = 1.0;
    if (radius > 0.5 - _VignetteSoftness)
    {
      if (radius < 0.5)
        right = pow(1.0 - ((radius - (0.5 - _VignetteSoftness)) / _VignetteSoftness), 1.5);
      else
        right = 0.0;
    }

    return 1.0 - (1.0 - left) * (1.0 - right);
  }

  float4 frag(v2f_img i) : COLOR
  {
    UNITY_SETUP_INSTANCE_ID(i);

    float3 pixel = SampleMainTexture(i.uv);

    // Digital TV.
#ifdef DIGITALTV_ENABLED
    float2 uv = DigitalTVNoise(i.uv, _DigitalTVNoiseThreshold, _DigitalTVNoiseMaxOffset);
#else
    float2 uv = i.uv;
#endif

    float3 final = SampleMainTexture(uv);

    // Edge.
#ifdef EDGE_ENABLED
    float gx = 0.0;
    gx += -1.0 * Lookup(uv, -1.0, -1.0, _EdgeStrength);
    gx += -2.0 * Lookup(uv, -1.0,  0.0, _EdgeStrength);
    gx += -1.0 * Lookup(uv, -1.0,  1.0, _EdgeStrength);
    gx +=  1.0 * Lookup(uv,  1.0, -1.0, _EdgeStrength);
    gx +=  2.0 * Lookup(uv,  1.0,  0.0, _EdgeStrength);
    gx +=  1.0 * Lookup(uv,  1.0,  1.0, _EdgeStrength);

    float gy = 0.0;
    gy += -1.0 * Lookup(uv, -1.0, -1.0, _EdgeStrength);
    gy += -2.0 * Lookup(uv,  0.0, -1.0, _EdgeStrength);
    gy += -1.0 * Lookup(uv,  1.0, -1.0, _EdgeStrength);
    gy +=  1.0 * Lookup(uv, -1.0,  1.0, _EdgeStrength);
    gy +=  2.0 * Lookup(uv,  0.0,  1.0, _EdgeStrength);
    gy +=  1.0 * Lookup(uv,  1.0,  1.0, _EdgeStrength);
    
    final += (gx * gx + gy * gy) * _EdgeTint;
#endif

    // Analog TV.
#ifdef ANALOGTV_ENABLED
    final *= lerp(sin(_ScreenParams.y * uv.y * _Scanline), final, 0.75 * _Strength);

    float3 grain = Noise(uv);

    grain = lerp(0.5, grain, _Noise * _Strength);
    grain = grain.r;
    final = Overlay(grain, final);
#else
    final *= lerp(0.0, final, 0.75);
#endif

    // Luminance.
    final *= (exp2(final) * _Exposure);

    // Color.
    final = lerp(SampleMainTexture(uv), final, _LuminanceAmount);

#ifdef COLORCONTROL_ADVANCED
    const float3 lumcoeff = float3(0.2126, 0.7152, 0.0722);

    float4 RGB_lum = float4(lumcoeff * _RGBLum, 0.0);
    final = dot(final, RGB_lum);

    if (final.r < 0.25)
      final.rgb = lerp(_Tint0, _Tint1, final.r * 4.0);
    else if (final.r < 0.5)
      final.rgb = lerp(_Tint1, _Tint2, (final.r - 0.25) * 4.0);
    else if (final.r < 0.75)
      final.rgb = lerp(_Tint2, _Tint3, (final.r - 0.5) * 4.0);
    else
      final.rgb = lerp(_Tint3, _Tint4, min(final.r - 0.75, 0.25) * 4.0);

    float3 intensity = dot(final.rgb, lumcoeff);
    final = lerp(intensity, final.rgb, _Saturation);

    final = (final - 0.5f) * _Contrast + 0.5f + _Brightness;
#else
    final = ((final.r + final.g + final.b) * 0.75);

    if (final.r < 0.25)
      final.rgb = lerp(_Tint0, _Tint1, final.r * 4.0);
    else if (final.r < 0.5)
      final.rgb = lerp(_Tint1, _Tint2, (final.r - 0.25) * 4.0);
    else if (final.r < 0.75)
      final.rgb = lerp(_Tint2, _Tint3, (final.r - 0.5) * 4.0);
    else
      final.rgb = lerp(_Tint3, _Tint4, min(final.r - 0.75, 0.25) * 4.0);
#endif

    // Vignette.
    float vignette = pow(4.0 * uv.y * (1.0 - uv.y), 0.15);
#ifdef VIGNETTE_SCREEN
    vignette *= VignetteScreen(uv);
#elif VIGNETTE_MONOCULAR
    vignette *= VignetteMonocular(uv);
#elif VIGNETTE_BINOCULAR
    vignette *= VignetteBinocular(uv);
#endif
    final *= vignette;

#ifdef UI_ENABLED
    final.rgb = lerp(final.rgb, DrawUI(final.rgb, uv), vignette);
#endif

    final = clamp(final, 0.0, 1.0);

    final = final + final - final * final;

    return float4(lerp(pixel, final, _Strength), 1.0);
  }

  ENDCG

  SubShader
  {
    ZTest Always
    Cull Off
    ZWrite Off
    Fog { Mode off }

    Pass
    {
      Name "Avanced Night Vision"

      CGPROGRAM
      #pragma target 3.0
      #pragma fragmentoption ARB_precision_hint_fastest
      
      #pragma multi_compile ___ VIGNETTE_NONE VIGNETTE_SCREEN VIGNETTE_MONOCULAR VIGNETTE_BINOCULAR
      #pragma multi_compile ___ COLORCONTROL_ADVANCED
      #pragma multi_compile ___ ANALOGTV_ENABLED
      #pragma multi_compile ___ DIGITALTV_ENABLED
      #pragma multi_compile ___ EDGE_ENABLED
      #pragma multi_compile ___ UI_ENABLED

      #pragma vertex vert_img
      #pragma fragment frag
      ENDCG
    }
  }

  Fallback off
}