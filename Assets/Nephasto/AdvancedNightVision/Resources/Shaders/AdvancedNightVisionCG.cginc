///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) Nephasto <hello@nephasto.com>. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef ADVANCEDNIGHTVISION_CGINC
#define ADVANCEDNIGHTVISION_CGINC

UNITY_DECLARE_SCREENSPACE_TEXTURE(_MainTex);
float4 _MainTex_ST;
float4 _MainTex_TexelSize;

#if SHADER_API_PS4
inline float2 lerp(float2 a, float2 b, float t)
{
  return lerp(a, b, (float2)t);
}

inline float3 lerp(float3 a, float3 b, float t)
{
  return lerp(a, b, (float3)t);
}

inline float4 lerp(float4 a, float4 b, float t)
{
  return lerp(a, b, (float4)t);
}
#endif

inline float3 SampleMainTexture(float2 uv)
{
  float3 pixel = 
#if defined(UNITY_SINGLE_PASS_STEREO)
    tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(uv, _MainTex_ST)).rgb;
#else
    UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, uv).rgb;
#endif

  return pixel;
}

inline float Sat(float t)
{
  return clamp(t, 0.0, 1.0);
}

inline float Linterp(float t)
{
  return Sat(1.0 - abs(2.0 * t - 1.0));
}

inline float Remap(float t, float a, float b)
{
  return Sat((t - a) / (b - a));
}

inline float Overlay(float s, float d)
{
  return (d < 0.5) ? 2.0 * s * d : 1.0 - 2.0 * (1.0 - s) * (1.0 - d);
}

inline float3 Overlay(float3 s, float3 d)
{
  float3 c;

  c.x = Overlay(s.x, d.x);
  c.y = Overlay(s.y, d.y);
  c.z = Overlay(s.z, d.z);

  return c;
}

inline float3 Screen(float3 s, float3 d)
{
  return s + d - s * d;
}

inline float Rand(float n)
{
  return frac(sin(n) * 43758.5453123);
}

inline float Rand2(float2 co)
{
  return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
}

inline float3 Noise(float2 uv)
{
  float2 c = _ScreenParams.x * float2(1.0, (_ScreenParams.y / _ScreenParams.x));
  float3 col = 0.0;

  float time = _Time.y * 0.05;

  return Rand2(float2((2.0 + time * 0.05) * floor(uv.x * c.x / 1.0) / c.x / 1.0, (2.0 + time) * floor(uv.y * c.y / 1.0) / c.y / 1.0));
}

inline float Trunc(float x, float numLevels)
{
  return floor(x * numLevels) / numLevels;
}

inline float2 Trunc(float2 x, float2 numLevels)
{
  return floor(x * numLevels) / numLevels;
}

// RGB -> YUV.
inline float3 RGB2YUV(float3 rgb)
{
  float3 yuv;
  yuv.x = dot(rgb, float3(0.299, 0.587, 0.114));
  yuv.y = dot(rgb, float3(-0.14713, -0.28886, 0.436));
  yuv.z = dot(rgb, float3(0.615, -0.51499, -0.10001));

  return yuv;
}

// YUV -> RGB.
inline float3 YUV2RGB(float3 yuv)
{
  float3 rgb;
  rgb.r = yuv.x + yuv.z * 1.13983;
  rgb.g = yuv.x + dot(float2(-0.39465, -0.58060), yuv.yz);
  rgb.b = yuv.x + yuv.y * 2.03211;

  return rgb;
}

inline float3 SpectrumOffset(float t)
{
  float3 ret;
  float lo = step(t, 0.5);
  float hi = 1.0 - lo;
  float w = Linterp(Remap(t, 1.0 / 6.0, 5.0 / 6.0));

  ret = float3(lo, 1.0, hi) * float3(1.0 - w, w, 1.0 - w);

  return pow(ret, 1.0 / 2.2);
}

inline float2 DigitalTVNoise(float2 uv, float threshold, float maxOffset)
{
  float modTime = fmod(_Time.y, 32.0);

  float thresholdGlitch = 1.0 - threshold;
  const float timeFreq = 16.0;

  const float minChangeFreq = 4.0;
  float ct = Trunc(modTime, minChangeFreq);
  float randChange = Rand2(Trunc(uv.yy, float2(16.0, 16.0)) + 150.0 * ct);

  float tf = timeFreq * randChange;

  float t = 5.0 * Trunc(modTime, tf);
  float randVT = 0.5 * Rand(Trunc(uv.yy + t, float2(11.0, 11.0)));
  randVT += 0.5 * Rand2(Trunc(uv.yy + t, float2(7.0, 7.0)));
  randVT = randVT * 2.0 - 1.0;
  randVT = sign(randVT) * clamp((abs(randVT) - thresholdGlitch) / (1.0 - thresholdGlitch), 0.0, 1.0);

  float2 uvNM = uv;
  uvNM = clamp(uvNM + float2(maxOffset * randVT, 0.0), 0.0, 1.0);

  float rnd = Rand2(float2(Trunc(modTime, 8.0), Trunc(modTime, 8.0)));
  uvNM.y = (rnd > lerp(1.0, 0.975, clamp(threshold, 0.0, 1.0))) ? 1.0 - uvNM.y : uvNM.y;

  return uvNM;
}

inline float Lookup(float2 p, float dx, float dy, float d)
{
  dx /= _ScreenParams.x;
  dy /= _ScreenParams.y;
  float2 uv = (p.xy + float2(dx * d, dy * d));
  float3 c = SampleMainTexture(uv.xy);

  return 0.2126 * c.r + 0.7152 * c.g + 0.0722 * c.b;
}

inline float d2y(float d)
{
  return 1.0 / (0.01 + d);
}
	
inline float Circle(float2 p, float r, float zoom)
{
  float d = distance(r, 0.45 * zoom);
  
  return d2y(1000.0 * d);
}

inline bool Line(float2 uv, float2 from, float2 to)
{
  return uv.x >= from.x && uv.x <= to.x && uv.y >= from.y && uv.y <= to.y;
}

inline float Grid(float2 p, const float e)
{
  float2 f = step(e, frac(p * 14.98));

  return f.x * f.y;
}

float2 _UICenter;
float _UIGridSize;
float4 _UIGridColor;
float _UICircle0Radius;
float _UICircle1Radius;
float _UICircle2Radius;
float4 _UICircleColor;
float _UICrossWidth;
float4 _UICrossColor;

inline float3 DrawUI(float3 pixel, float2 uv)
{
  float aspect = _ScreenParams.x / _ScreenParams.y;
  float2 uvc = _UICenter - (uv * 2.0 - 1.0);

  uvc.x *= aspect;
  float dc = length(uvc) * 0.5;

  float y = 0.0;
  if (_UICircle2Radius > 0.0)
    y += Circle(uvc, dc, _UICircle2Radius);

  if (_UICircle1Radius > 0.0)
    y += Circle(uvc, dc, _UICircle1Radius);

  if (_UICircle0Radius > 0.0)
    y += Circle(uvc, dc, _UICircle0Radius);

  pixel = lerp(pixel, _UICircleColor.rgb, y * _UICircleColor.a);
  
  if (_UIGridSize > 0.0)
    pixel = lerp(pixel, _UIGridColor.rgb, Grid(uvc, _UIGridSize) * _UIGridColor.a);

  if (_UICrossWidth > 0.0)
  {
    float width = _UICrossWidth * _MainTex_TexelSize.y;
    float height = _UICrossWidth * _MainTex_TexelSize.x * aspect;

    if (Line(uvc, float2(-width, -1.0), float2(width, 1.0)) || Line(uvc, float2(-4.0, -height), float2(4.0, height)))
      pixel = lerp(pixel, _UICrossColor.rgb, _UICrossColor.a);
  }

  return pixel;
}
#endif