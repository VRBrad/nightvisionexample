﻿using Nephasto.AdvancedNightVisionAsset;
using UnityEngine;

/// <summary>
/// On/off trigger.
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class NightVisionTrigger : MonoBehaviour
{
  [SerializeField]
  private string filterTag = "Player";

  [SerializeField]
  private bool triggerOn = true;

  private AdvancedNightVision nightVision;

  private BoxCollider boxCollider;

  private void Awake()
  {
    boxCollider = GetComponent<BoxCollider>();
    boxCollider.isTrigger = true;
  }

  private void OnTriggerEnter(Collider other)
  {
    if (nightVision == null)
      nightVision = FindObjectOfType<AdvancedNightVision>();

    if (nightVision.enabled == true && nightVision.On != triggerOn && other.tag.Equals(filterTag) == true)
      nightVision.On = triggerOn;
  }

  private void OnTriggerExit(Collider other)
  {
    if (nightVision == null)
      nightVision = FindObjectOfType<AdvancedNightVision>();

    if (nightVision.enabled == true && nightVision.On != !triggerOn && other.tag.Equals(filterTag) == true)
      nightVision.On = !triggerOn;
  }

  private void OnDrawGizmos()
  {
    if (boxCollider != null)
    {
      Gizmos.color = Color.green;

      Gizmos.DrawWireCube(boxCollider.center + transform.position, Vector3.Scale(boxCollider.size, transform.lossyScale));    
    }
  }
}