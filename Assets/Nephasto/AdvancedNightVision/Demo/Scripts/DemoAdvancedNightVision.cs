﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Nephasto.AdvancedNightVisionAsset;

/// <summary>
/// Advanced Night Vision demo.
/// </summary>
public sealed class DemoAdvancedNightVision : MonoBehaviour
{
  [SerializeField]
  private bool guiShow = true;

  [SerializeField]
  private SimpleFirstPersonController simpleFirstPersonController = null;

  [SerializeField]
  private List<Light> ignoreLights = new List<Light>();

  [SerializeField]
  private AudioClip musicClip = null;
  
  [SerializeField]
  private AudioClip nightVisionOnClip = null;

  [SerializeField]
  private AudioClip nightVisionOffClip = null;

  [SerializeField]
  private AudioClip breathClip = null;

  [SerializeField]
  private Text text = null;

  [SerializeField]
  private string credits = "";

  private enum Presets
  {
    One,
    Two,
    Three
  }
  
  private AdvancedNightVision advancedNightVision;

  private bool menuOpen = false;

  private const float guiMargen = 10.0f;
  private const float guiWidth = 425.0f;

  private float updateInterval = 0.5f;
  private float accum = 0.0f;
  private int frames = 0;
  private float timeleft;
  private float fps = 0.0f;

  private GUIStyle effectNameStyle;
  private GUIStyle menuStyle;
  private GUIStyle boxStyle;
  private GUIStyle creditsStyle;

  private int toolbarMultipass = 0;
  private int toolbarRenderSize = 0;
  private int toolbarColorControl = 1;
  private int toolbarDistortion = 2;

  private Vector2 scrollPosition = Vector2.zero;

  private AudioSource audioSourceFX; 
  private AudioSource audioSourceMusic; 
  private AudioSource audioSourceBreath;

  private Presets preset = Presets.Three;
  private IEnumerator presetCoroutine;  
  
  private void OnEnable()
  {
    Camera selectedCamera = null;
    Camera[] cameras = GameObject.FindObjectsOfType<Camera>();

    for (int i = 0; i < cameras.Length; ++i)
    {
      if (cameras[i].enabled == true)
      {
        selectedCamera = cameras[i];

        break;
      }
    }

    if (selectedCamera != null)
    {
      advancedNightVision = selectedCamera.gameObject.GetComponent<AdvancedNightVision>();
      if (advancedNightVision == null)
        advancedNightVision = selectedCamera.gameObject.AddComponent<AdvancedNightVision>();

      advancedNightVision.On = false;

      advancedNightVision.IgnoreLights = ignoreLights;

      if (nightVisionOnClip != null && nightVisionOffClip != null)
      {
        audioSourceFX = this.gameObject.AddComponent<AudioSource>();
        audioSourceFX.volume = 0.25f;
        audioSourceFX.loop = false;

        advancedNightVision.SwitchOnTime = nightVisionOnClip.length * 0.75f;
        advancedNightVision.SwitchOffTime = nightVisionOffClip.length * 0.5f;

        advancedNightVision.OnSwitch += SwitchEvent;
        
        Preset(Presets.One);
      }

      if (breathClip != null)
      {
        audioSourceBreath = this.gameObject.AddComponent<AudioSource>();
        audioSourceBreath.volume = 1.0f;
        audioSourceBreath.clip = breathClip;
        audioSourceBreath.loop = true;
      }

      if (musicClip != null)
      {
        audioSourceMusic = this.gameObject.AddComponent<AudioSource>();
        audioSourceMusic.volume = 0.2f;
        audioSourceMusic.clip = musicClip;
        audioSourceMusic.loop = true;
        
        audioSourceMusic.Play();
      }

      if (text != null)
      {
        text.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        this.StartCoroutine(FadeImage(true));
      }

      if (simpleFirstPersonController != null)
        simpleFirstPersonController.OnStartMoving += () =>
        {
          if (audioSourceBreath != null)
            audioSourceBreath.PlayDelayed(2.0f);

          if (text != null)
            this.StartCoroutine(FadeImage(false));
        };
    }
    else
      Debug.LogWarning("No camera found.");
  }

  private IEnumerator FadeImage(bool fadeIn)
  {
    float time = 1.0f;
    while (time > 0.0f)
    {
      text.color = new Color(1.0f, 1.0f, 1.0f, fadeIn == true ? 1.0f - time : time);

      time -= Time.deltaTime;
          
      yield return null;
    }

    text.color = new Color(1.0f, 1.0f, 1.0f, fadeIn == true ? 1.0f : 0.0f);
  }

  private IEnumerator ChangePresetCoroutine(Presets newPreset)
  {
    if (advancedNightVision.On == true)
    {
      advancedNightVision.On = false;
    
      yield return new WaitForSeconds(advancedNightVision.SwitchOffTime);
    
      Preset(newPreset);

      advancedNightVision.On = true;
    }
    else
      Preset(newPreset);
  }
  
  private void Preset(Presets newPreset)
  {
    if (newPreset != preset)
    {
      preset = newPreset;

      advancedNightVision.ResetDefaultValues();

      advancedNightVision.ShaderPass = ShaderPasses.MultiPass;

      switch (preset)
      {
        case Presets.One:
          advancedNightVision.ColorGradient = ColorGradients.Green;
          advancedNightVision.VignetteType = VignetteTypes.Screen;
          advancedNightVision.VignetteScale = 8.0f;
          advancedNightVision.VignetteSoftness = 0.25f;

          advancedNightVision.DigitalTV = true;

          advancedNightVision.Edge = false;

          advancedNightVision.UI = true;
          advancedNightVision.UICrossWidth = 1.0f;
          advancedNightVision.UICrossColor = new Color(0.0f, 0.9f, 0.0f, 0.05f);
          advancedNightVision.UIGridColor = new Color(0.0f, 0.9f, 0.0f, 0.2f);
          advancedNightVision.UICircle0Radius = advancedNightVision.UICircle1Radius = advancedNightVision.UICircle2Radius = 0.0f;
          break;

        case Presets.Two:
          advancedNightVision.Glow = 0.0f;

          advancedNightVision.ColorGradient = ColorGradients.Cyan;

          advancedNightVision.Edge = true;
          advancedNightVision.EdgeTint = Color.white;
          
          advancedNightVision.VignetteType = VignetteTypes.Binocular;
          advancedNightVision.VignetteScale = 12.0f;
          advancedNightVision.VignetteSoftness = 0.25f;

          advancedNightVision.ChromaticAberration = -6.0f;
          advancedNightVision.BarrelDistortion = 9.0f;

          advancedNightVision.Noise = 10;
          advancedNightVision.Scanline = 1.75f;
          
          advancedNightVision.DigitalTV = false;
          advancedNightVision.AnalogTV = true;

          advancedNightVision.UI = false;
          break;
      
        case Presets.Three:
          advancedNightVision.ColorGradient = ColorGradients.Red;

          advancedNightVision.Edge = true;
          advancedNightVision.EdgeTint = Color.red;
          
          advancedNightVision.VignetteType = VignetteTypes.Monocular;
          advancedNightVision.VignetteScale = 10.0f;
          advancedNightVision.VignetteSoftness = 0.25f;

          advancedNightVision.BlurPasses = 1;
          advancedNightVision.ChromaticAberration = -4.0f;
          advancedNightVision.BarrelDistortion = -7.0f;
          
          advancedNightVision.DigitalTV = false;
          advancedNightVision.AnalogTV = true;
          advancedNightVision.Noise = 5.0f;
          advancedNightVision.Scanline = 0.0f;

          advancedNightVision.UI = true;
          advancedNightVision.UICrossWidth = 0.0f;
          advancedNightVision.UIGridSize = 0.0f;
          advancedNightVision.UICircleColor = new Color(1.0f, 0.0f, 0.0f, 0.2f);
          break;
      }
    }
  }
  
  private void Update()
  {
    timeleft -= Time.deltaTime;
    accum += Time.timeScale / Time.deltaTime;
    frames++;

    if (timeleft <= 0.0f)
    {
      fps = accum / frames;
      timeleft = updateInterval;
      accum = 0.0f;
      frames = 0;
    }

    if (Input.GetKeyUp(KeyCode.Tab) == true)
      guiShow = !guiShow;

    if (Input.GetKeyUp(KeyCode.Alpha1) == true && preset != Presets.One)
      StartCoroutine(ChangePresetCoroutine(Presets.One));
    else if (Input.GetKeyUp(KeyCode.Alpha2) == true && preset != Presets.Two)
      StartCoroutine(ChangePresetCoroutine(Presets.Two));
    else if (Input.GetKeyUp(KeyCode.Alpha3) == true && preset != Presets.Three)
      StartCoroutine(ChangePresetCoroutine(Presets.Three));
  }

  private void OnGUI()
  {
    if (advancedNightVision == null)
      return;

    if (effectNameStyle == null)
    {
      effectNameStyle = new GUIStyle(GUI.skin.textArea)
      {
        alignment = TextAnchor.MiddleCenter,
        fontSize = 22
      };
    }

    if (menuStyle == null)
      menuStyle = new GUIStyle(GUI.skin.textArea)
      {
        normal = { background = MakeTex(2, 2, new Color(0.4f, 0.4f, 0.4f, 0.5f)) },
        alignment = TextAnchor.MiddleCenter,
        fontSize = 13
      };

    if (boxStyle == null)
    {
      boxStyle = new GUIStyle(GUI.skin.box);
      boxStyle.normal.background = MakeTex(2, 2, new Color(0.2f, 0.2f, 0.2f, 0.5f));
      boxStyle.focused.textColor = Color.red;
    }

    if (creditsStyle == null)
    {
      creditsStyle = new GUIStyle(GUI.skin.label);
      creditsStyle.wordWrap = true;
    }

    if (guiShow == false)
      return;

    GUILayout.BeginHorizontal(boxStyle, GUILayout.Width(Screen.width));
    {
      GUILayout.Space(guiMargen);

      if (GUILayout.Button("OPTIONS", menuStyle, GUILayout.Width(80.0f)) == true)
        menuOpen = !menuOpen;

      GUILayout.FlexibleSpace();

      GUILayout.Label("ADVANCED NIGHT VISION", menuStyle, GUILayout.Width(200.0f));

      GUILayout.FlexibleSpace();

      if (GUILayout.Button("MUTE", menuStyle, GUILayout.Width(50.0f)) == true)
        AudioListener.volume = 1.0f - AudioListener.volume;

      GUILayout.Space(guiMargen);
      
      if (fps < 30.0f)
        GUI.contentColor = Color.yellow;
      else if (fps < 15.0f)
        GUI.contentColor = Color.red;
      else
        GUI.contentColor = Color.green;

      GUILayout.Label(fps.ToString("000"), menuStyle, GUILayout.Width(40.0f));

      GUI.contentColor = Color.white;

      GUILayout.Space(guiMargen);
    }
    GUILayout.EndHorizontal();

    if (menuOpen == true)
    {
      GUILayout.BeginVertical(boxStyle, GUILayout.Width(guiWidth));
      {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);

        GUILayout.Space(guiMargen);

        // ON / OFF
        GUILayout.BeginVertical(boxStyle);
        {
          GUILayout.BeginHorizontal();
          {
            GUILayout.Label("Night Vision");
            advancedNightVision.On = GUILayout.Toolbar(advancedNightVision.On == true ? 0 : 1, new[] { "ON", "OFF" }) == 0 ? true : false;
          }
          GUILayout.EndHorizontal();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(guiMargen);

        // Presets
        GUILayout.BeginHorizontal(boxStyle);
        {
          Presets newPreset = (Presets)GUILayout.Toolbar((int)preset, new[] { "Preset 1", "Preset 2", "Preset 3" });
          if (newPreset != preset)
          {
            if (presetCoroutine != null)
              this.StopCoroutine(presetCoroutine);

            this.StartCoroutine(presetCoroutine = ChangePresetCoroutine(newPreset));
          }
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(guiMargen);

        // Parameters.
        GUILayout.BeginVertical(boxStyle);
        {
          GUILayout.Label("Quality");

          GUILayout.BeginHorizontal();
          {
            GUILayout.Label(" Shader");

            toolbarMultipass = advancedNightVision.ShaderPass == ShaderPasses.OnePass ? 0 : 1;
            toolbarMultipass = GUILayout.Toolbar(toolbarMultipass, new[] { "One pass", "Multi-pass" });

            advancedNightVision.ShaderPass = toolbarMultipass == 0 ? ShaderPasses.OnePass : ShaderPasses.MultiPass;
          }
          GUILayout.EndHorizontal();

          GUILayout.BeginHorizontal();
          {
            GUILayout.Label(" Render");
            toolbarRenderSize = GUILayout.Toolbar(toolbarRenderSize, new[] { "Normal", "Half", "Quarter" });

            switch (toolbarRenderSize)
            {
              case 0: advancedNightVision.RenderResolution = RenderTextureResolutions.Normal; break;
              case 1: advancedNightVision.RenderResolution = RenderTextureResolutions.Half; break;
              case 2: advancedNightVision.RenderResolution = RenderTextureResolutions.Quarter; break;
            }
          }
          GUILayout.EndHorizontal();

          if (advancedNightVision.ShaderPass == ShaderPasses.MultiPass)
          {
            advancedNightVision.BlurPasses = (int)HorizontalSlider(" Blur", advancedNightVision.BlurPasses, 0.0f, 10.0f);

            advancedNightVision.Glow = (int)HorizontalSlider(" Glow", advancedNightVision.Glow, 0.0f, 25.0f);
          }
        }
        GUILayout.EndVertical();

        GUILayout.Space(guiMargen);

        GUILayout.BeginVertical(boxStyle);
        {
          GUILayout.Label("Color");

          // Color Gradients.
          GUILayout.BeginHorizontal(boxStyle);
          {
            advancedNightVision.ColorGradient = (ColorGradients)GUILayout.Toolbar((int)advancedNightVision.ColorGradient, new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" });
          }
          GUILayout.EndHorizontal();

          GUILayout.BeginHorizontal();
          {
            GUILayout.Label(" Mode");
            toolbarColorControl = GUILayout.Toolbar(toolbarColorControl, new[] { "Simple", "Advanced" });

            advancedNightVision.ColorControl = (ColorControls)toolbarColorControl;
          }
          GUILayout.EndHorizontal();

          advancedNightVision.LuminanceAmount = HorizontalSlider(" Amount", advancedNightVision.LuminanceAmount, 0.0f, 1.0f);

          advancedNightVision.Exposure = HorizontalSlider(" Exposure", advancedNightVision.Exposure, 0.0f, 10.0f);

          if (advancedNightVision.ColorControl == ColorControls.Advanced)
          {
            advancedNightVision.Brightness = HorizontalSlider(" Brightness", advancedNightVision.Brightness, -1.0f, 1.0f);

            advancedNightVision.Contrast = HorizontalSlider(" Contrast", advancedNightVision.Contrast, -1.0f, 1.0f);

            advancedNightVision.Saturation = HorizontalSlider(" Saturation", advancedNightVision.Saturation, 0.0f, 2.0f);

            GUILayout.Label(" Offset", GUILayout.Width(70));

            Vector3 rgbOffset = advancedNightVision.RGBOffset;

            rgbOffset.x = HorizontalSlider("    R", rgbOffset.x, -100.0f, 100.0f);
            rgbOffset.y = HorizontalSlider("    G", rgbOffset.y, -100.0f, 100.0f);
            rgbOffset.z = HorizontalSlider("    B", rgbOffset.z, -100.0f, 100.0f);

            advancedNightVision.RGBOffset = rgbOffset;
          }
        }
        GUILayout.EndVertical();

        GUILayout.Space(guiMargen);

        GUILayout.BeginVertical(boxStyle);
        {
          advancedNightVision.Edge = GUILayout.Toggle(advancedNightVision.Edge, " Edge");

          if (advancedNightVision.Edge == true)
            advancedNightVision.EdgeStrength = HorizontalSlider("Strength", advancedNightVision.EdgeStrength, 0.0f, 5.0f);
        }
        GUILayout.EndVertical();

        GUILayout.Space(guiMargen);

        GUILayout.BeginVertical(boxStyle);
        {
          advancedNightVision.UI = GUILayout.Toggle(advancedNightVision.UI, " UI");
        }
        GUILayout.EndVertical();

        GUILayout.Space(guiMargen);

        GUILayout.BeginVertical(boxStyle);
        {
          GUILayout.Label("Glitches");

          GUILayout.BeginHorizontal();
          {
            GUILayout.Label(" Vignette");
            advancedNightVision.VignetteType = (VignetteTypes)GUILayout.Toolbar((int)advancedNightVision.VignetteType, new[] { "None", "Screen", "Monocular", "Binocular" });
          }
          GUILayout.EndHorizontal();

          if (advancedNightVision.VignetteType != VignetteTypes.None)
          {
            advancedNightVision.VignetteScale = HorizontalSlider("    Scale", advancedNightVision.VignetteScale, 0.0f, 100.0f);

            advancedNightVision.VignetteSoftness = HorizontalSlider("    Softness", advancedNightVision.VignetteSoftness, 0.0f, 10.0f);
          }

          GUILayout.BeginHorizontal();
          {
            GUILayout.Label(" Distortion");
            toolbarDistortion = GUILayout.Toolbar(toolbarDistortion, new[] { "None", "Simple", "Advanced" });

            advancedNightVision.ChromaticAberrationType = (ChromaticAberrationTypes)toolbarDistortion;
          }
          GUILayout.EndHorizontal();

          if (advancedNightVision.ShaderPass != ShaderPasses.OnePass && advancedNightVision.ChromaticAberrationType != ChromaticAberrationTypes.None)
          {
            advancedNightVision.ChromaticAberration = HorizontalSlider("    Aberration", advancedNightVision.ChromaticAberration, -100.0f, 100.0f);

            if (advancedNightVision.ChromaticAberrationType == ChromaticAberrationTypes.Advanced)
            {
              advancedNightVision.DistortionsPasses = (int)HorizontalSlider("    Passes", advancedNightVision.DistortionsPasses, 3.0f, 24.0f);

              advancedNightVision.BarrelDistortion = (int)HorizontalSlider("    Barrel", advancedNightVision.BarrelDistortion, -100.0f, 100.0f);
            }
          }

          advancedNightVision.Noise = HorizontalSlider(" Noise", advancedNightVision.Noise, 0.0f, 10.0f);

          advancedNightVision.Scanline = HorizontalSlider(" Scanlines", advancedNightVision.Scanline, 0.0f, 2.0f);
        }
        GUILayout.EndVertical();

        GUILayout.Space(guiMargen);

        GUILayout.BeginHorizontal(boxStyle);
        {
          GUILayout.Label("1,2,3 - Presets\nTAB - Hide/Show gui");

          if (GUILayout.Button("Open Web") == true)
            Application.OpenURL(AdvancedNightVision.Documentation);
        }
        GUILayout.EndHorizontal();

        if (string.IsNullOrEmpty(credits) == false)
        {
          GUILayout.Space(guiMargen);

          GUILayout.BeginHorizontal(boxStyle);
          {
            GUILayout.TextArea(credits.Replace("|", "\n"), creditsStyle);
          }
          GUILayout.EndVertical();
        }

        GUILayout.Space(guiMargen);

        GUILayout.EndScrollView();
      }
      GUILayout.EndVertical();
    }
  }

  private void SwitchEvent(bool on)
  {
    audioSourceFX.Stop();
    audioSourceFX.clip = on == true ? nightVisionOnClip : nightVisionOffClip;
    audioSourceFX.Play();
  }

  private float HorizontalSlider(string label, float val, float min, float max)
  {
    GUILayout.BeginHorizontal();
    {
      GUILayout.Label(label, GUILayout.Width(90));
      val = GUILayout.HorizontalSlider(val, min, max);

    }
    GUILayout.EndHorizontal();

    return val;
  }
  
  private Texture2D MakeTex(int width, int height, Color col)
  {
    Color[] pix = new Color[width * height];
    for (int i = 0; i < pix.Length; ++i)
      pix[i] = col;

    Texture2D result = new Texture2D(width, height);
    result.SetPixels(pix);
    result.Apply();

    return result;
  }
}

