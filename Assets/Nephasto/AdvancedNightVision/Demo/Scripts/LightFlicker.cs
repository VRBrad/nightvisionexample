﻿using System.Collections.Generic;

using UnityEngine;

/// <summary>
/// Light flicker.
/// </summary>
[RequireComponent(typeof(Light))]
public class LightFlicker : MonoBehaviour
{
  [SerializeField]
  private float minIntensity = 0.0f;

  [SerializeField]
  private float maxIntensity = 1.0f;

  [SerializeField]
  private Material materialEmissive = null;

  // Lower values = sparks, higher = lantern.
  [SerializeField, Range(1, 50)]
  private int smoothing = 5;

  private Light flicker = null;

  private Queue<float> smoothQueue;
  
  private float lastSum = 0.0f;

  private void Start()
  {
    flicker = this.gameObject.GetComponent<Light>();
    if (flicker != null)
    {
      smoothQueue = new Queue<float>(smoothing);

      maxIntensity = Mathf.Max(flicker.intensity, maxIntensity);
    }
    else
      this.enabled = flicker != null;
  }

  private void Update()
  {
    if (flicker.enabled == true)
    {
      while (smoothQueue.Count >= smoothing)
        lastSum -= smoothQueue.Dequeue();

      float newVal = Random.Range(minIntensity, maxIntensity);
      smoothQueue.Enqueue(newVal);
      lastSum += newVal;

      flicker.intensity = lastSum / (float)smoothQueue.Count;

      if (materialEmissive != null)
        materialEmissive.SetColor("_EmissionColor", Color.white * flicker.intensity);
    }
  }
}